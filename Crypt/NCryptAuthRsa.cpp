#include "NCryptAuthRsa.h"

bigint GcdEx(const bigint& a, const bigint& b, bigint& x, bigint& y) {
	if (a == 0) {
		x = 0; y = 1;
		return b;
	}
	bigint x1, y1;
	bigint d = GcdEx(b % a, a, x1, y1);
	x = y1 - (b / a) * x1;
	y = x1;
	return d;
}

size_t NCryptAuthRsa::MessageSize() {
	return key_n_.Size();
}

bool NCryptAuthRsa::SetPrivateKey(std::vector<uint64_t> private_key) {
	if (private_key.size() < 4)
		return false;

	uint64_t n1 = private_key[0];

	if (private_key.size() < n1 + 1)
		return false;
	vector<uint64_t> arr1(n1);
	for (size_t i = 0; i < n1; i++) {
		arr1[i] = private_key[i + 1];
	}

	if (private_key.size() < n1 + 2)
		return false;
	uint64_t n2 = private_key[n1 + 1];

	if (private_key.size() < n1 + 1 + n2 + 1)
		return false;
	vector<uint64_t> arr2(n2);
	for (size_t i = 0; i < n2; i++) {
		arr2[i] = private_key[i + n1 + 2];
	}

	key_d_ = bigint(arr1, 1);
	key_n_ = bigint(arr2, 1);

	return true;
}

bool NCryptAuthRsa::SetPublicKey(std::vector<uint64_t> public_key) {
	if (public_key.size() < 4)
		return false;

	uint64_t n1 = public_key[0];

	if (public_key.size() < n1 + 1)
		return false;
	vector<uint64_t> arr1(n1);
	for (size_t i = 0; i < n1; i++) {
		arr1[i] = public_key[i + 1];
	}

	if (public_key.size() < n1 + 2)
		return false;
	uint64_t n2 = public_key[n1 + 1];

	if (public_key.size() < n1 + 1 + n2 + 1)
		return false;
	vector<uint64_t> arr2(n2);
	for (size_t i = 0; i < n2; i++) {
		arr2[i] = public_key[i + n1 + 2];
	}

	key_e_ = bigint(arr1, 1);
	key_n_ = bigint(arr2, 1);

	return true;
}

bool NCryptAuthRsa::Test(std::vector<uint64_t> encrypted_hash_data, std::vector<uint64_t> hash_data) {
	bigint h(hash_data, 1);
	h = h % key_n_;
	bigint m(encrypted_hash_data, 1);
	if (key_n_ == 0 || key_e_ == 0)
		return false;
	m = PowMod(m, key_e_, key_n_);
	return m == h;
}

bool NCryptAuthRsa::Generate(int64_t size, std::vector<uint64_t>& private_key, std::vector<uint64_t>& public_key, NRandomGen& rg) {
	bigint p1, p2, n, eiler_n, e, d;
	p1 = RandomPrime(static_cast<int>(size), rg);
	p2 = RandomPrime(static_cast<int>(size), rg);
	n = p1 * p2;
	eiler_n = (p1 - 1) * (p2 - 1);

	vector<uint64_t> arr;
	arr.resize((eiler_n.FirstBitPos() + 64) / 64);
	bigint gx, gy;
	bigint gres = 0;
	while (gres != 1) {

		rg.GetRandom(arr.data(), arr.size() * sizeof(uint64_t));
		e = bigint(arr, 1);
		e = e % eiler_n;
		if (e < (bigint(1) << static_cast<int>(size / 2))) {
			e = e + (bigint(1) << static_cast<int>(size / 2));
		}

		gres = GcdEx(e, eiler_n, gx, gy);
	}
	d = (gx + eiler_n) % eiler_n;
	size_t narr_size = n.Size();
	auto narr = n.Data();
	size_t earr_size = e.Size();
	auto earr = e.Data();
	size_t darr_size = d.Size();
	auto darr = d.Data();

	bigint m = 2;
	m = PowMod(m, e, n);
	m = PowMod(m, d, n);
	if (m != 2)
		throw;

	private_key.resize(darr_size + narr_size + 2);

	private_key[0] = darr_size;
	for (size_t i = 0; i < darr_size; i++)
		private_key[i + 1] = darr[i];

	private_key[darr_size + 1] = narr_size;
	for (size_t i = 0; i < narr_size; i++)
		private_key[i + darr_size + 2] = narr[i];

	public_key.resize(earr_size + narr_size + 2);

	public_key[0] = earr_size;
	for (size_t i = 0; i < earr_size; i++)
		public_key[i + 1] = earr[i];

	public_key[earr_size + 1] = narr_size;
	for (size_t i = 0; i < narr_size; i++)
		public_key[i + earr_size + 2] = narr[i];

	return true;
}

bool NCryptAuthRsa::Crypt(std::vector<uint64_t> hash_data, std::vector<uint64_t>& encrypted_hash_data) {

	bigint h = bigint(hash_data, 1);

	h = h % key_n_;
	h = PowMod(h, key_d_, key_n_);

	size_t data_size = h.Size();
	auto data = h.Data();
	encrypted_hash_data = vector<uint64_t>(data, data + data_size);
	return true;
}
