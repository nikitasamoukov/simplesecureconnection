#pragma once
#include "bigint.h"
#include "NCryptBase.h"

class  NCryptAuthRsa final : public NCryptAuthentication {
	bigint key_n_, key_e_, key_d_;
public:
	// Size of encrypted message(uint64_t count)
	NCRYPT_DLL_API size_t MessageSize() override;
	NCRYPT_DLL_API bool SetPrivateKey(std::vector<uint64_t> private_key) override;
	NCRYPT_DLL_API bool SetPublicKey(std::vector<uint64_t> public_key) override;
	// Test equals encrypted hash and computed hash
	NCRYPT_DLL_API bool Test(std::vector<uint64_t> encrypted_hash_data, std::vector<uint64_t> hash_data) override;
	// Generate public and private keys
	NCRYPT_DLL_API bool Generate(int64_t size, std::vector<uint64_t>& private_key, std::vector<uint64_t>& public_key, NRandomGen& rg) override;
	// Encrypt hash using private key
	NCRYPT_DLL_API bool Crypt(std::vector<uint64_t> hash_data, std::vector<uint64_t>& encrypted_hash_data) override;
};
