#include "NCryptBase.h"

bigint RandomNumberGen(NRandomGen& rg, size_t size, bool add_bit) {
	if (size < 1)
		throw;
	if (size == 1) {
		if (add_bit)
			return 1;
		char ch;
		if (!rg.GetRandom(&ch, 1))
			throw;
		return ch % 2;
	}

	vector<uint64_t> arr;
	arr.resize((size - 1) / 64 + 1, 0);

	if (!rg.GetRandom(arr.data(), arr.size() * sizeof(uint64_t)))
		throw;

	if (add_bit)
		arr[(size - 1) / 64] |= (uint64_t)1 << ((size - 1) % 64);

	if (size % 64 != 0)
		arr[(size - 1) / 64] &= (((uint64_t)1 << (size - (size - 1) / 64 * 64)) - 1);

	return bigint(arr, 1);
}

bigint RandomPrime(size_t size, NRandomGen& rg) {

	bigint res = NextPrime(RandomNumberGen(rg, size));

	if (res.Size() > (size - 1) / 64 + 1 ||
		((size % 64 != 0) && res.Data()[(size - 1) / 64] > ((uint64_t)1 << ((size) % 64)))
		) {
		res = NextPrime(res >> 1);
	}
	return res;
}

bigint RandomExtraPrime(size_t size, NRandomGen& rg) {
	bigint res = NextPrimeEx(RandomNumberGen(rg, size));

	if (res.Size() > (size - 1) / 64 + 1 ||
		((size % 64 != 0) && res.Data()[(size - 1) / 64] > ((uint64_t)1 << ((size) % 64)))
		) {
		res = NextPrimeEx(res >> 1);
	}
	return res;
}
