#pragma once

#include <cstdint>
#include <vector>

#include "bigint.h"
#include "NCryptDefines.h"


class NCryptConnection;

// Cryptographic random
class NCRYPT_DLL_API NRandomGen {
public:
	virtual bool GetRandom(void*, int64_t) = 0;
	NRandomGen() = default;
	virtual ~NRandomGen() = default;
	NRandomGen(NRandomGen const&) = delete;
	NRandomGen& operator =(NRandomGen const&) = delete;
	NRandomGen(NRandomGen&&) = delete;
	NRandomGen& operator=(NRandomGen&&) = delete;
};

// Symmetric cryptography
class NCRYPT_DLL_API NCryptSymmetric {
public:
	// Data scale in bytes
	virtual uint64_t GetDataScale() = 0;
	// Key size in bytes
	virtual uint64_t GetKeySize() = 0;
	virtual bool SetKey(const std::vector<uint64_t>&) = 0;
	virtual bool Encode(void* src, void* dst, uint64_t sz) = 0;
	virtual bool Decode(void* src, void* dst, uint64_t sz) = 0;
	NCryptSymmetric() = default;
	virtual ~NCryptSymmetric() = default;
	NCryptSymmetric(NCryptSymmetric const&) = delete;
	NCryptSymmetric& operator =(NCryptSymmetric const&) = delete;
	NCryptSymmetric(NCryptSymmetric&&) = delete;
	NCryptSymmetric& operator=(NCryptSymmetric&&) = delete;
};

// Auth cryptography
class  NCRYPT_DLL_API NCryptAuthentication {
public:
	// Size of encrypted message(uint64_t count)
	virtual size_t MessageSize() = 0;
	virtual bool SetPrivateKey(std::vector<uint64_t>) = 0;
	virtual bool SetPublicKey(std::vector<uint64_t>) = 0;
	// Test equals encrypted hash and computed hash
	virtual bool Test(std::vector<uint64_t> encrypted_hash_data, std::vector<uint64_t> hash_data) = 0;
	// Generate public and private keys
	virtual bool Generate(int64_t, std::vector<uint64_t>&, std::vector<uint64_t>&, NRandomGen&) = 0;
	// Encrypt hash using private key
	virtual bool Crypt(std::vector<uint64_t>, std::vector<uint64_t>&) = 0;
	NCryptAuthentication() = default;
	virtual ~NCryptAuthentication() = default;
	NCryptAuthentication(NCryptAuthentication const&) = delete;
	NCryptAuthentication& operator =(NCryptAuthentication const&) = delete;
	NCryptAuthentication(NCryptAuthentication&&) = delete;
	NCryptAuthentication& operator=(NCryptAuthentication&&) = delete;
};

// Cryptographic hash function
class NCRYPT_DLL_API NCryptHash {
public:
	// Hash calculate
	virtual bool Hash(const void*, size_t, void*) = 0;
	// Hash size
	virtual int HashSize() = 0;
	NCryptHash() = default;
	virtual ~NCryptHash() = default;
	NCryptHash(NCryptHash const&) = delete;
	NCryptHash& operator =(NCryptHash const&) = delete;
	NCryptHash(NCryptHash&&) = delete;
	NCryptHash& operator=(NCryptHash&&) = delete;
};

// Cryptographic sync shared secret
class NCRYPT_DLL_API NCryptSync {
public:
	virtual int Step1(const vector<uint64_t>& d, vector<uint64_t>& b, int t, NRandomGen& rg) = 0;
	virtual int Step2(const vector<uint64_t>& d, vector<uint64_t>& b, int t, NRandomGen& rg) = 0;
	virtual size_t StepSize(int type, int step) = 0;
	virtual vector<uint64_t> GetResult() = 0;
	virtual size_t RandomBitsCount() = 0;
	NCryptSync() = default;
	virtual ~NCryptSync() = default;
	NCryptSync(NCryptSync const&) = delete;
	NCryptSync& operator =(NCryptSync const&) = delete;
	NCryptSync(NCryptSync&&) = delete;
	NCryptSync& operator=(NCryptSync&&) = delete;
};

// Network connection 
class NCRYPT_DLL_API NCryptConnection {
public:
	virtual bool ConnectToServer(const char* ip, uint16_t port, int timeout_seconds = -1) = 0;
	virtual bool ListenClients(uint16_t port) = 0;
	virtual NCryptConnection* AcceptClient(int timeout_seconds = -1) = 0;
	virtual bool SendV(void*, uint64_t) = 0;
	virtual bool RecvV(void*, uint64_t) = 0;
	virtual void Disconnect() = 0;
	NCryptConnection() = default;
	virtual ~NCryptConnection() = default;
	NCryptConnection(NCryptConnection const&) = delete;
	NCryptConnection& operator =(NCryptConnection const&) = delete;
	NCryptConnection(NCryptConnection&&) = delete;
	NCryptConnection& operator=(NCryptConnection&&) = delete;
};

// Generate random number. size in bits.
extern NCRYPT_DLL_API bigint RandomNumberGen(NRandomGen& rg, size_t size, bool add_bit = true);

// Generate random prime number. size in bits.
extern NCRYPT_DLL_API bigint RandomPrime(size_t size, NRandomGen& rg);

// Generate extra random number. num and (num-1)/2 is prime. size in bits.
extern NCRYPT_DLL_API bigint RandomExtraPrime(size_t size, NRandomGen& rg);
