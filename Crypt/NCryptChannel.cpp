#include "NCryptChannel.h"

uint8_t ReverseBits(uint8_t a) {
	// 76543210
	// 6 4 2 0 
	//  7 5 3 1
	// 67452301
	//   67  23
	// 45  01
	// 45670123
	// 0123
	//     4561
	// 01234567
	a = (a & 0x55) << 1 | (a & 0xAA) >> 1;
	a = (a & 0x33) << 2 | (a & 0xCC) >> 2;
	a = (a & 0x0F) << 4 | (a & 0xF0) >> 4;
	return a;
}

NCryptChannel::NCryptChannel(NCryptStr s) {
	crypt_ = std::move(s);
	if (crypt_.hash->HashSize() % 8 != 0) {
		throw std::invalid_argument("Hash size % 8 != 0");
	}
}

bool NCryptChannel::Connect(const std::string& ip, int port, int timeout) {
	bool b = crypt_.connect->ConnectToServer(ip.c_str(), port, timeout);
	if (!b) {
		return false;
	}
	my_type_ = Type::CLIENT;
	return true;
}

bool NCryptChannel::ServerConnect(std::shared_ptr<NCryptConnection> connection) {
	crypt_.connect = std::move(connection);
	if (!crypt_.connect) {
		return false;
	}
	my_type_ = Type::SERVER;
	return true;
}

bool NCryptChannel::SyncKey(bool is_test_result) {
	int res = 0;

	vector<uint64_t> keyres;
	size_t rand_bits = crypt_.sync->RandomBitsCount();
	if (rand_bits == 0) {
		return false;
	}

	// Create session key space
	uint64_t curr_key_bits = 0;
	bool key_ready = false;
	uint64_t key_need_size = crypt_.symm->GetKeySize() * 8;
	keyres.resize((crypt_.symm->GetKeySize() + 7) / 8);

	do {
		vector<uint64_t> temp_vec0, temp_vec1;
		int st = 0;

		if (my_type_ == Type::CLIENT) {
			do {
				temp_vec0.resize(crypt_.sync->StepSize(1, st));

				if (!temp_vec0.empty()) {

					vector<uint64_t> arrh, arrauth;
					arrh.resize(crypt_.hash->HashSize() / 8);
					arrauth.resize(crypt_.auth->MessageSize(), 0);
					crypt_.connect->RecvV(temp_vec0.data(), temp_vec0.size() * 8);
					crypt_.connect->RecvV(arrauth.data(), arrauth.size() * 8);
					crypt_.hash->Hash(temp_vec0.data(), temp_vec0.size() * 8, arrh.data());
					int resauth = crypt_.auth->Test(arrauth, arrh);

					if (resauth == 0) {
						return false;
					}
				}

				res = crypt_.sync->Step1(temp_vec0, temp_vec1, st, *crypt_.rg);
				if (!temp_vec1.empty() && res != -1) {
					crypt_.connect->SendV(temp_vec1.data(), temp_vec1.size() * 8);

				}
				st++;
			} while (res == 0);
		}
		if (my_type_ == Type::SERVER) {
			do {
				temp_vec0.resize(crypt_.sync->StepSize(2, st));

				if (!temp_vec0.empty()) {
					bool connect_res = crypt_.connect->RecvV(temp_vec0.data(), temp_vec0.size() * 8);
					if (!connect_res) {
						return false;
					}
				}

				res = crypt_.sync->Step2(temp_vec0, temp_vec1, st, *crypt_.rg);
				if (!temp_vec1.empty() && res != -1) {
					vector<uint64_t> arrh, arrauth;
					arrh.resize(crypt_.hash->HashSize() / 8);
					crypt_.hash->Hash(temp_vec1.data(), temp_vec1.size() * 8, arrh.data());
					crypt_.auth->Crypt(arrh, arrauth);
					arrauth.resize(crypt_.auth->MessageSize(), 0);
					crypt_.connect->SendV(temp_vec1.data(), temp_vec1.size() * 8);
					crypt_.connect->SendV(arrauth.data(), arrauth.size() * 8);
				}
				st++;
			} while (res == 0);
		}

		if (res != 1)
			return false;

		vector<uint64_t> arr2 = crypt_.sync->GetResult();
		arr2.resize((rand_bits + 63) / 64, 0);

		uint64_t addd = curr_key_bits / 64;
		uint64_t addm = curr_key_bits % 64;

		if (rand_bits % 64 != 0)
			arr2[(rand_bits - 1) / 64] &= ((uint64_t(1) << (rand_bits % 64)) - 1);


		for (size_t i = 0; i < arr2.size() && (i + addd) < keyres.size(); i++)
			keyres[i + addd] |= arr2[i] << addm;
		if (addm != 0)
			for (size_t i = 0; i < arr2.size() && (i + addd + 1) < keyres.size(); i++)
				keyres[i + addd + 1] |= arr2[i] >> (64 - addm);

		curr_key_bits += rand_bits;
		if (curr_key_bits >= key_need_size) {
			key_ready = true;
		}
	} while (!key_ready);

	if (!is_test_result) {
		return true;
	}

	bool test_result = true;
	if (my_type_ == Type::CLIENT) {
		vector<uint8_t> testrandval, testrandvalret, mess;
		mess.resize(crypt_.symm->GetDataScale(), 0);
		testrandval.resize(crypt_.symm->GetDataScale(), 0);
		testrandvalret.resize(crypt_.symm->GetDataScale(), 0);
		crypt_.rg->GetRandom(testrandval.data(), testrandval.size());

		crypt_.symm->SetKey(keyres);

		crypt_.symm->Encode(testrandval.data(), mess.data(), mess.size());

		crypt_.connect->SendV(mess.data(), mess.size());
		crypt_.connect->RecvV(mess.data(), mess.size());

		crypt_.symm->Decode(mess.data(), testrandvalret.data(), mess.size());

		for (size_t i = 0; i < testrandval.size(); i++) {
			if ((testrandval[i] ^ testrandvalret[i]) != 0xFF) {
				test_result = false;
			}
		}

		for (size_t i = 0; i < testrandval.size() / 2; i++) {
			std::swap(testrandvalret[i], testrandvalret[testrandval.size() - 1 - i]);
		}
		for (size_t i = 0; i < testrandval.size(); i++)
			testrandvalret[i] = ReverseBits(testrandvalret[i]);

		crypt_.symm->Encode(testrandvalret.data(), mess.data(), mess.size());

		crypt_.connect->SendV(mess.data(), mess.size());

		symm_chain_data_ = testrandvalret;
		symm_chain_data_decode_ = testrandvalret;
	}

	if (my_type_ == Type::SERVER) {
		vector<uint8_t> testrandval, testrandvalret, mess;
		mess.resize(crypt_.symm->GetDataScale(), 0);
		testrandval.resize(crypt_.symm->GetDataScale(), 0);
		testrandvalret.resize(crypt_.symm->GetDataScale(), 0);

		crypt_.connect->RecvV(mess.data(), mess.size());

		crypt_.symm->SetKey(keyres);

		crypt_.symm->Decode(mess.data(), testrandval.data(), mess.size());

		for (auto& val : testrandval) {
			val = ~val;
		}

		crypt_.symm->Encode(testrandval.data(), mess.data(), mess.size());

		crypt_.connect->SendV(mess.data(), mess.size());

		for (size_t i = 0; i < testrandval.size() / 2; i++) {
			std::swap(testrandval[i], testrandval[testrandval.size() - 1 - i]);
		}
		for (auto& val : testrandval) {
			val = ReverseBits(val);
		}

		crypt_.connect->RecvV(mess.data(), mess.size());

		crypt_.symm->Decode(mess.data(), testrandvalret.data(), mess.size());

		for (size_t i = 0; i < testrandval.size(); i++) {
			if (testrandval[i] != testrandvalret[i])
				test_result = false;
		}
		symm_chain_data_ = testrandvalret;
		symm_chain_data_decode_ = testrandvalret;
	}
	return test_result;
}

void NCryptChannel::SCrypt(void* data, size_t size) {
	size_t scale = crypt_.symm->GetDataScale();
	if (size % scale != 0)
		throw;
	auto lbytedata = reinterpret_cast<uint8_t*>(data);

	for (size_t i = 0; i < size / scale; i++) {
		for (size_t r = 0; r < symm_chain_data_.size(); r++)
			lbytedata[i * scale + r] ^= symm_chain_data_[r];
		crypt_.symm->Encode(lbytedata + i * scale, lbytedata + i * scale, scale);
		for (size_t r = 0; r < symm_chain_data_.size(); r++)
			symm_chain_data_[r] = lbytedata[i * scale + r];
	}
}

void NCryptChannel::SEncrypt(void* data, size_t size) {
	size_t scale = crypt_.symm->GetDataScale();
	if (size % scale != 0) {
		throw;
	}

	auto lbytedata = reinterpret_cast<uint8_t*>(data);
	vector<uint8_t> temp(symm_chain_data_decode_.size());
	for (size_t i = 0; i < size / scale; i++) {
		for (size_t r = 0; r < symm_chain_data_decode_.size(); r++)
			temp[r] = lbytedata[i * scale + r];

		crypt_.symm->Decode(lbytedata + i * scale, lbytedata + i * scale, scale);

		for (size_t r = 0; r < symm_chain_data_decode_.size(); r++)
			lbytedata[i * scale + r] ^= symm_chain_data_decode_[r];

		symm_chain_data_decode_ = temp;
	}
}

bool NCryptChannel::SendData(const void* data, size_t size) {
	if (crypt_.symm->GetDataScale() % 8 != 0) {
		throw;
	}

	//uint64_t hash
	//uint64_t size
	//uint64_t size(duplicate)
	//uint8_t some data
	//uint8_t some random bits to keep size scale uint64_t

	size_t sizepadd = 3 * 8 + size;
	if (sizepadd % crypt_.symm->GetDataScale() != 0)
		sizepadd += crypt_.symm->GetDataScale() - (sizepadd % crypt_.symm->GetDataScale());
	vector<uint64_t> vecsend(sizepadd / 8, 0);

	vecsend[1] = size;
	vecsend[2] = size;

	auto lbytevec = reinterpret_cast<uint8_t*>(&(vecsend[3]));
	const auto lbytedata = reinterpret_cast<const uint8_t*>(data);
	for (size_t i = 0; i < size; i++)
		lbytevec[i] = lbytedata[i];

	if (((3 * 8 + size) % crypt_.symm->GetDataScale()) > 0)
		crypt_.rg->GetRandom(&lbytevec[size], crypt_.symm->GetDataScale() - ((3 * 8 + size) % crypt_.symm->GetDataScale()));

	uint64_t xorhash = 0;
	for (size_t i = 2; i < vecsend.size(); i++)
		xorhash ^= vecsend[i];

	vecsend[0] = xorhash;

	SCrypt(vecsend.data(), vecsend.size() * 8);

	return crypt_.connect->SendV(vecsend.data(), vecsend.size() * 8);
}

bool NCryptChannel::RecvData(vector<uint8_t>& vec) {
	if (crypt_.symm->GetDataScale() % 8 != 0)
		throw;
	size_t sizehpadd = 3 * 8;
	if (sizehpadd % (crypt_.symm->GetDataScale()) != 0)
		sizehpadd += crypt_.symm->GetDataScale() - (sizehpadd % crypt_.symm->GetDataScale());

	vector<uint64_t> vechead(sizehpadd / 8, 0);

	int resrecv = crypt_.connect->RecvV(vechead.data(), vechead.size() * 8);
	if (!resrecv)
		return false;

	SEncrypt(vechead.data(), vechead.size() * 8);

	if (vechead[1] != vechead[2])
		return false;
	size_t size = vechead[1];

	size_t sizepadd = 3 * 8 + size;
	if (sizepadd % crypt_.symm->GetDataScale() != 0) {
		sizepadd += crypt_.symm->GetDataScale() - (sizepadd % crypt_.symm->GetDataScale());
	}

	if (sizepadd / 8 - vechead.size() > 0) {
		vechead.resize(sizepadd / 8, 0);

		bool res_recv = crypt_.connect->RecvV(vechead.data() + sizehpadd / 8, vechead.size() * 8 - sizehpadd);
		if (!res_recv) {
			return false;
		}

		SEncrypt(vechead.data() + sizehpadd / 8, vechead.size() * 8 - sizehpadd);
	}

	uint64_t xorhash = 0;
	for (size_t i = 2; i < vechead.size(); i++)
		xorhash ^= vechead[i];

	if (xorhash != vechead[0])
		return false;

	auto lbytevec = reinterpret_cast<uint8_t*>(&(vechead[3]));
	vec.resize(size);
	for (size_t i = 0; i < size; i++) {
		vec[i] = lbytevec[i];
	}

	return true;
}
