#pragma once
#include "NCryptBase.h"

// Struct with pointers to cryptographic algorithms realizations
struct NCryptStr {
	std::shared_ptr<NCryptAuthentication> auth;
	std::shared_ptr<NCryptHash> hash;
	std::shared_ptr<NCryptSync> sync;
	std::shared_ptr<NCryptSymmetric> symm;
	std::shared_ptr<NCryptConnection> connect;
	std::shared_ptr<NRandomGen> rg;
};

// Secure channel
// send/recieve binary packets
class NCryptChannel {
	enum class Type {
		SERVER,
		CLIENT
	};
	NCryptStr crypt_;
	vector<uint8_t> symm_chain_data_;
	vector<uint8_t> symm_chain_data_decode_;
	Type my_type_ = Type::CLIENT;

	void SCrypt(void* data, size_t size);
	void SEncrypt(void* data, size_t size);

public:

	// Need provide algorithms, for create channel
	NCRYPT_DLL_API explicit NCryptChannel(NCryptStr s);

	// Connect to server
	NCRYPT_DLL_API bool Connect(const std::string& ip, int port, int timeout = -1);

	// Connect from server side
	NCRYPT_DLL_API bool ServerConnect(std::shared_ptr<NCryptConnection> connection);

	// Synchronize session key
	NCRYPT_DLL_API bool SyncKey(bool is_test_result = true);

	// Send data
	NCRYPT_DLL_API bool SendData(const void* data, size_t size);

	// Receive data
	NCRYPT_DLL_API bool RecvData(vector<uint8_t>& vec);
};
