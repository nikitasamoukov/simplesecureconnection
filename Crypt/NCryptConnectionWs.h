#pragma once
#include "NCryptBase.h"

#include <winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>
#include <cstdio>
#include <mutex>

#pragma comment (lib, "Ws2_32.lib")

class NCryptConnectionWs final : public NCryptConnection {

	struct addrinfo net_addr_ { 0 };
	SOCKET connect_socket_ = INVALID_SOCKET;
	SOCKET listen_socket_ = INVALID_SOCKET;

	explicit NCryptConnectionWs(SOCKET connect_socket) : connect_socket_(connect_socket) {}
public:
	static bool WsInit() {
		WSAData data;
		int res = WSAStartup(MAKEWORD(2, 2), &data);
		return res == 0;
	}

	static void WsClear() {
		WSACleanup();
	}

	NCryptConnectionWs() = default;

	bool ConnectToServer(const char* ip, uint16_t port, int timeout_seconds = -1) override {
		if (!WsInit())
			return false;
		if (timeout_seconds != -1) {
			throw std::exception("Not implemented");
		}

		ZeroMemory(&net_addr_, sizeof(net_addr_));
		net_addr_.ai_family = AF_UNSPEC;
		net_addr_.ai_socktype = SOCK_STREAM;
		net_addr_.ai_protocol = IPPROTO_TCP;

		string port_str = std::to_string(port);

		struct addrinfo* result = nullptr;

		int res = getaddrinfo(ip, port_str.c_str(), &net_addr_, &result);
		if (res != 0)
			return false;

		for (auto ptr = result; ptr != nullptr; ptr = ptr->ai_next) {

			// Create a SOCKET for connecting to server
			connect_socket_ = socket(ptr->ai_family, ptr->ai_socktype,
				ptr->ai_protocol);
			if (connect_socket_ == INVALID_SOCKET) {
				freeaddrinfo(result);
				return false;
			}

			// Connect to server.
			res = connect(connect_socket_, ptr->ai_addr, (int)ptr->ai_addrlen);
			if (res == SOCKET_ERROR) {
				closesocket(connect_socket_);
				connect_socket_ = INVALID_SOCKET;
				continue;
			}
			break;
		}

		freeaddrinfo(result);

		if (connect_socket_ == INVALID_SOCKET) {
			return false;
		}

		return true;
	}

	void Disconnect() override {
		closesocket(connect_socket_);
		connect_socket_ = INVALID_SOCKET;
		closesocket(listen_socket_);
		listen_socket_ = INVALID_SOCKET;
	}

	bool ListenClients(uint16_t port) override {
		if (!WsInit())
			return false;

		if (listen_socket_ != INVALID_SOCKET) {
			Disconnect();
		}

		struct addrinfo* result = nullptr;
		struct addrinfo hints;

		string port_str = std::to_string(port);

		ZeroMemory(&hints, sizeof(hints));
		hints.ai_family = AF_INET;
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_protocol = IPPROTO_TCP;
		hints.ai_flags = AI_PASSIVE;

		// Resolve the server address and port
		int res = getaddrinfo(nullptr, port_str.c_str(), &hints, &result);
		if (res != 0) {
			Disconnect();
			return false;
		}

		// Create a SOCKET for connecting to server
		listen_socket_ = listen_socket_ = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
		if (listen_socket_ == INVALID_SOCKET) {
			freeaddrinfo(result);
			Disconnect();
			return false;
		}

		// Setup the TCP listening socket
		res = bind(listen_socket_, result->ai_addr, (int)result->ai_addrlen);
		if (res == SOCKET_ERROR) {
			printf("bind failed with error: %d\n", WSAGetLastError());
			freeaddrinfo(result);
			Disconnect();
			return false;
		}

		freeaddrinfo(result);

		res = listen(listen_socket_, SOMAXCONN);
		if (res == SOCKET_ERROR) {
			Disconnect();
			return false;
		}
		return true;
	}

	NCryptConnection* AcceptClient(int timeout_seconds = -1) override {
		if (timeout_seconds == -1) {
			SOCKET connect_socket = accept(listen_socket_, nullptr, nullptr);
			auto connection = new NCryptConnectionWs(connect_socket);
			return connection;
		}

		FD_SET read_set;
		FD_ZERO(&read_set);
		FD_SET(listen_socket_, &read_set);

		struct timeval timeout { 0 };
		timeout.tv_sec = timeout_seconds;

		if (select(0, &read_set, nullptr, nullptr, &timeout) == 1) {
			// Accept a client socket
			SOCKET connect_socket = accept(listen_socket_, nullptr, nullptr);
			if (connect_socket == INVALID_SOCKET) {
				Disconnect();
				return nullptr;
			}
			auto connection = new NCryptConnectionWs(connect_socket);
			return connection;
		}
		return nullptr;
	}

	bool SendV(void* send_buf, uint64_t size) override {
		if (size > INT_MAX) {
			Disconnect();
			return false;
		}
		int offs = 0;
		while (offs < (int)size) {
			int res = send(connect_socket_, reinterpret_cast<const char*>(send_buf) + offs, (int)size - offs, 0);
			if (res == SOCKET_ERROR) {
				Disconnect();
				return false;
			}
			if (res > 0)
				offs += res;
		}
		return true;
	}

	bool RecvV(void* recv_buf, uint64_t size) override {
		if (size > INT_MAX) {
			throw std::invalid_argument("Size max is 2147483647.");
		}

		int recv_buf_offset = 0;

		do {
			int res = recv(connect_socket_, reinterpret_cast<char*>(recv_buf) + recv_buf_offset, (int)size - recv_buf_offset, 0);
			if (res == 0) {
				Disconnect();
				return false;
			}
			if (res < 0) {
				Disconnect();
				return false;
			}
			recv_buf_offset += res;
		} while (recv_buf_offset < (int)size);

		return true;
	}

	~NCryptConnectionWs() override {
		Disconnect();
	}
};
