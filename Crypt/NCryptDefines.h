#pragma once

#ifdef _WIN32
#ifdef _NCRYPT_DLL_COMPILE
#define NCRYPT_DLL_API __declspec(dllexport)
#else
#define NCRYPT_DLL_API __declspec(dllimport) 
#endif
#else
#define NCRYPT__DLL_API
#endif