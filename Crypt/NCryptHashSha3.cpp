#include "NCryptHashSha3.h"

#include <array>

uint64_t Rot(uint64_t x, int n) {
	n = n % 64;
	return (((x << n) | (x >> (64 - n))));
}

constexpr size_t sha3_512_hash_size = 512 / 8;

class FState {
public:
	std::array<uint64_t, 25> A_{};
	size_t state_sz_, new_sz_;
	explicit FState(size_t size) {
		new_sz_ = (1600 - 2 * size * 8) / 8;
		state_sz_ = size;
		if ((new_sz_ % 8) != 0) {
			throw std::invalid_argument("Bad initialize");
		}
	}
	void AddData(const uint8_t* buf) {
		auto buf_64 = reinterpret_cast<const uint64_t*>(buf);
		for (size_t i = 0; i < new_sz_ / 8; i++) {
			A_[i] ^= buf_64[i];
		}
	}
};

const uint64_t RC[] = {
	0x0000000000000001L, 0x0000000000008082L, 0x800000000000808aL,
	0x8000000080008000L, 0x000000000000808bL, 0x0000000080000001L,
	0x8000000080008081L, 0x8000000000008009L, 0x000000000000008aL,
	0x0000000000000088L, 0x0000000080008009L, 0x000000008000000aL,
	0x000000008000808bL, 0x800000000000008bL, 0x8000000000008089L,
	0x8000000000008003L, 0x8000000000008002L, 0x8000000000000080L,
	0x000000000000800aL, 0x800000008000000aL, 0x8000000080008081L,
	0x8000000000008080L, 0x0000000080000001L, 0x8000000080008008L
};

const int R[] = {
	0, 1, 62, 28, 27, 36, 44, 6, 55, 20, 3, 10, 43,
	25, 39, 41, 45, 15, 21, 8, 18, 2, 61, 56, 14
};

uint64_t RotLeft(uint64_t x, int n) {
	return (x << n) | (x >> (64 - n));
}

uint64_t RotRight(uint64_t x, int n) {
	return (x >> n) | (x << (64 - n));
}

void KeccakF(FState& s) {
	std::array<uint64_t, 25> B{};
	std::array<uint64_t, 25> C{};
	std::array<uint64_t, 25> D{};
	std::array<uint64_t, 25>& A = s.A_;

	for (size_t n = 0; n < 24; n++) {

		//theta
		for (size_t i = 0; i < 5; i++)
			C[i] = A[i + 5 * 0] ^ A[i + 5 * 1] ^ A[i + 5 * 2] ^ A[i + 5 * 3] ^ A[i + 5 * 4];

		for (size_t i = 0; i < 5; i++)
			D[i] = C[(i + 4) % 5] ^ RotLeft(C[(i + 1) % 5], 1);

		for (size_t i = 0; i < 5; i++)
			for (size_t r = 0; r < 5; r++)
				A[i + r * 5] ^= D[i];

		//rho pi
		for (size_t x = 0; x < 5; x++)
			for (size_t y = 0; y < 5; y++)
				B[y + ((2 * x + 3 * y) % 5) * 5] = RotLeft(A[x + y * 5], R[x + y * 5]);
		//chi
		for (size_t x = 0; x < 5; x++)
			for (size_t y = 0; y < 5; y++)
				A[x + y * 5] = B[x + y * 5] ^ ((~B[(x + 1) % 5 + y * 5]) & B[(x + 2) % 5 + y * 5]);

		//ota
		A[0] ^= RC[n];
	}
}

void HashSha3(const void* src, uint64_t src_size, void* des) {
	FState st(sha3_512_hash_size);
	vector<uint8_t> buf(st.new_sz_, 0);
	//0x06 0x80

	for (size_t i = 0; i < src_size / st.new_sz_; i++) {
		st.AddData(static_cast<const uint8_t*>(src) + i * st.new_sz_);
		KeccakF(st);
	}
	size_t mm = (src_size % st.new_sz_);

	// Add ending anyway
	{
		size_t offs = src_size / st.new_sz_ * st.new_sz_;
		for (size_t i = 0; i < st.new_sz_; i++)
			buf[i] = 0;
		for (size_t i = 0; i < mm; i++)
			buf[i] = *(static_cast<const uint8_t*>(src) + offs + i);
		buf[mm] |= 0x06;
		buf[st.new_sz_ - 1] |= 0x80;

		st.AddData(buf.data());

		KeccakF(st);
	}

	memcpy(des, st.A_.data(), sha3_512_hash_size);
}

bool NCryptHashSha3::Hash(const void* src, size_t src_size, void* des) {
	HashSha3(src, src_size, des);
	return true;
}

int NCryptHashSha3::HashSize() {
	return sha3_512_hash_size;
}
