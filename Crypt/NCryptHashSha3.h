#pragma once
#include "NCryptBase.h"

class NCRYPT_DLL_API NCryptHashSha3 final : public NCryptHash {
public:
	bool Hash(const void*, size_t, void*) override;
	int HashSize() override;
	~NCryptHashSha3() = default;
	NCryptHashSha3() = default;
	NCryptHashSha3(const NCryptHashSha3&) = delete;
	NCryptHashSha3(NCryptHashSha3&&) = delete;
	NCryptHashSha3& operator =(const NCryptHashSha3&) = delete;
	NCryptHashSha3& operator =(NCryptHashSha3&&) = delete;
};

