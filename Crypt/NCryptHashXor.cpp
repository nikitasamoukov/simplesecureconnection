#include "NCryptHashXor.h"

bool NCryptHashXor::Hash(const void* src, size_t src_size, void* res) {
	auto src_array = reinterpret_cast<const uint8_t*>(src);
	auto res_array = reinterpret_cast<uint8_t*>(res);

	memset(res_array, 0, size_);

	for (size_t i = 0; i < src_size; i++) {
		res_array[i % size_] ^= src_array[i];
	}

	return true;
}

bool NCryptHashXor::SetSize(uint64_t size) {
	if (size == 0)
		return false;
	size_ = size;
	return true;
}
