#pragma once
#include "NCryptBase.h"

class NCryptHashXor :public NCryptHash {
	uint64_t size_ = 128;
public:
	NCRYPT_DLL_API bool Hash(const void*, size_t, void*) override;
	// Set size hash
	NCRYPT_DLL_API bool SetSize(uint64_t size);
	NCRYPT_DLL_API ~NCryptHashXor() = default;
};
