#include "NCryptRandom.h"
#include <random>

#ifdef _WIN64
#include <windows.h>
#undef max
#undef min
#include <bcrypt.h>
#include <winternl.h>

#pragma comment( lib, "Bcrypt.lib" )

bool NRandomGenWin::GetRandom(void* data, int64_t size) {
	NTSTATUS Status = BCryptGenRandom(
		nullptr,
		(PUCHAR)data,
		static_cast<ULONG>(size),
		BCRYPT_USE_SYSTEM_PREFERRED_RNG);

	if (!NT_SUCCESS(Status)) {
		return false;
	}
	return true;
}
#endif 

NRandomGenSeed::NRandomGenSeed(uint64_t seed) {
	generator_ = new std::mt19937_64(seed);
}

bool NRandomGenSeed::GetRandom(void* data, int64_t size) {
	for (int i = 0; i < size; i++) {
		((uint8_t*)data)[i] = static_cast<uint8_t>(
			(*reinterpret_cast<std::mt19937_64*>(generator_))()
			);
	}
	return true;
}