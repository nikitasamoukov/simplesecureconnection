#pragma once

#include "NCryptBase.h"

#ifdef _WIN64
// Cryptographic random Windows implementation
class NCRYPT_DLL_API NRandomGenWin final : public NRandomGen{
	bool GetRandom(void*, int64_t) override;
};
#define N_BASIC_RG const_cast<NRandomGenWin&>(static_cast<const NRandomGenWin&>(NRandomGenWin()))
#endif

class NRandomGenSeed final : public NRandomGen {
	 void* generator_;
public:
	NCRYPT_DLL_API explicit NRandomGenSeed(uint64_t seed = 100);
	NCRYPT_DLL_API bool GetRandom(void* data, int64_t size) override;
};
#define N_RG_SEED const_cast<NRandomGenSeed&>(static_cast<const NRandomGenSeed&>(NRandomGenSeed()))
