#include "NCryptSymIdea.h"

uint64_t NCryptSymIdea::GetDataScale() {
	return 8;
}

uint64_t NCryptSymIdea::GetKeySize() {
	return 128 / 8;
}

uint16_t OpXor(uint16_t a, uint16_t b) {
	return a ^ b;
}

uint16_t OpAdd(uint16_t a, uint16_t b) {
	return a + b;
}

uint16_t OpMul(uint32_t a, uint32_t b) {
	if (a == 0)
		a += 0x10000;
	if (b == 0)
		b += 0x10000;
	return (a * b) % 0x10001;
}

void GcdEx(int a, int b, int& x, int& y) {

	int p = 1, q = 0, r = 0, s = 1;

	while (a && b) {
		if (a >= b) {
			a = a - b;
			p = p - r;
			q = q - s;
		} else {
			b = b - a;
			r = r - p;
			s = s - q;
		}
	}
	if (a) {
		x = p;
		y = q;
	} else {
		x = r;
		y = s;
	}
}

uint16_t OpInv(uint16_t a) {
	if (a == 0)
		return 0;
	int x, y;
	GcdEx(a, 0x10001, x, y);
	return (x + 0x10001) % 0x10001;
}

bool NCryptSymIdea::SetKey(const std::vector<uint64_t>& key) {
	if (key.size() != 128 / 64)
		return false;
	have_decrypt_key_ = false;

	vector<uint64_t> temp_key = key;

	uint16_t* ilink = reinterpret_cast<uint16_t*>(temp_key.data());


	temp_key[0] =
		((temp_key[0] & 0x000000000000ffff) << 48) |
		((temp_key[0] & 0x00000000ffff0000) << 16) |
		((temp_key[0] & 0x0000ffff00000000) >> 16) |
		((temp_key[0] & 0xffff000000000000) >> 48);
	temp_key[1] =
		((temp_key[1] & 0x000000000000ffff) << 48) |
		((temp_key[1] & 0x00000000ffff0000) << 16) |
		((temp_key[1] & 0x0000ffff00000000) >> 16) |
		((temp_key[1] & 0xffff000000000000) >> 48);

	for (int i = 0; i < 6; i++) {
		encrypt_key_[i * 8 + 0] = ilink[3];
		encrypt_key_[i * 8 + 1] = ilink[2];
		encrypt_key_[i * 8 + 2] = ilink[1];
		encrypt_key_[i * 8 + 3] = ilink[0];
		encrypt_key_[i * 8 + 4] = ilink[7];
		encrypt_key_[i * 8 + 5] = ilink[6];
		encrypt_key_[i * 8 + 6] = ilink[5];
		encrypt_key_[i * 8 + 7] = ilink[4];

		uint64_t k1 = (temp_key[0] << 25) | (temp_key[1] >> (64 - 25));
		uint64_t k2 = (temp_key[1] << 25) | (temp_key[0] >> (64 - 25));
		temp_key[0] = k1;
		temp_key[1] = k2;
	}

	encrypt_key_[6 * 8 + 0] = ilink[3];
	encrypt_key_[6 * 8 + 1] = ilink[2];
	encrypt_key_[6 * 8 + 2] = ilink[1];
	encrypt_key_[6 * 8 + 3] = ilink[0];

	return true;
}


uint64_t IdeaEncode(uint64_t src, uint16_t* K) {
	auto D0 = uint16_t(src);
	auto D1 = uint16_t(src >> 16);
	auto D2 = uint16_t(src >> 32);
	auto D3 = uint16_t(src >> 48);

	for (int i = 0; i < 8; i++) {
		uint16_t p01, p02, p03, p04;
		uint16_t p11, p12;
		uint16_t p21, p22;
		uint16_t p31, p32;
		uint16_t
			K1 = K[i * 6 + 0],
			K2 = K[i * 6 + 1],
			K3 = K[i * 6 + 2],
			K4 = K[i * 6 + 3],
			K5 = K[i * 6 + 4],
			K6 = K[i * 6 + 5];

		p01 = OpMul(D0, K1);
		p02 = OpAdd(D1, K2);
		p03 = OpAdd(D2, K3);
		p04 = OpMul(D3, K4);

		p11 = OpXor(p01, p03);
		p12 = OpXor(p02, p04);

		p21 = OpMul(p11, K5);
		p22 = OpAdd(p12, p21);

		p32 = OpMul(p22, K6);
		p31 = OpAdd(p32, p21);

		D0 = OpXor(p01, p32);
		D1 = OpXor(p03, p32);
		D2 = OpXor(p02, p31);
		D3 = OpXor(p04, p31);
	}

	D0 = OpMul(D0, K[8 * 6 + 0]);
	D1 = OpAdd(D1, K[8 * 6 + 2]);
	D2 = OpAdd(D2, K[8 * 6 + 1]);
	D3 = OpMul(D3, K[8 * 6 + 3]);

	return D0 | uint64_t(D2) << 16 | uint64_t(D1) << 32 | uint64_t(D3) << 48;
}

bool NCryptSymIdea::Encode(void* src, void* dst, uint64_t sz) {
	if (sz % (64 / 8) != 0)
		return false;

	for (int i = 0; i < sz / 8; i++)
		reinterpret_cast<uint64_t*>(dst)[i] = IdeaEncode(reinterpret_cast<uint64_t*>(src)[i], encrypt_key_);

	return true;
}

bool NCryptSymIdea::Decode(void* src, void* dst, uint64_t sz) {
	if (sz % (64 / 8) != 0)
		return false;

	if (!have_decrypt_key_)
		GenDecryptionKey();

	for (int i = 0; i < sz / 8; i++)
		reinterpret_cast<uint64_t*>(dst)[i] = IdeaEncode(reinterpret_cast<uint64_t*>(src)[i], decrypt_key_);

	return true;
}

void NCryptSymIdea::GenDecryptionKey() {
	have_decrypt_key_ = true;

	for (int i = 0, r = 6 * 8; i < 52; i++, r++) {
		switch (i % 6) {
		case 0:
			decrypt_key_[r] = OpInv(encrypt_key_[i]);
			break;
		case 1:
			decrypt_key_[r] = -encrypt_key_[i + 1];
			break;
		case 2:
			decrypt_key_[r] = -encrypt_key_[i - 1];
			break;
		case 3:
			decrypt_key_[r] = OpInv(encrypt_key_[i]);
			r -= 6;
			break;
		case 4:
			decrypt_key_[r] = encrypt_key_[i];
			break;
		case 5:
			decrypt_key_[r] = encrypt_key_[i];
			r -= 6;
			break;
		default:
			throw std::exception("Unreachable code");
		}
	}
	std::swap(decrypt_key_[1], decrypt_key_[2]);
	std::swap(decrypt_key_[1 + 8 * 6], decrypt_key_[2 + 8 * 6]);
}



