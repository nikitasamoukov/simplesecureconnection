#pragma once

#include "NCryptBase.h"

class NCRYPT_DLL_API NCryptSymIdea final : public NCryptSymmetric {
	uint16_t encrypt_key_[6 * 8 + 4] = { 0 };
	uint16_t decrypt_key_[6 * 8 + 4] = { 0 };
	bool have_decrypt_key_ = false;
public:
	uint64_t GetDataScale() override;
	uint64_t GetKeySize() override;
	bool SetKey(const std::vector<uint64_t>&) override;
	bool Encode(void*, void*, uint64_t) override;
	bool Decode(void*, void*, uint64_t) override;
	void GenDecryptionKey();

	~NCryptSymIdea() = default;
	NCryptSymIdea() = default;
	NCryptSymIdea(NCryptSymIdea const&) = delete;
	NCryptSymIdea& operator =(NCryptSymIdea const&) = delete;
	NCryptSymIdea(NCryptSymIdea&&) = delete;
	NCryptSymIdea& operator=(NCryptSymIdea&&) = delete;
};

