#include "NCryptSyncDiffHelm.h"

void NCryptSyncDiffHelm::SetPrime(const bigint& prime) {
	p = prime;
	have_prime_ = true;
}

void NCryptSyncDiffHelm::SetPrimeSize(int sz) {
	prime_size_ = sz;
	prime_data_size_ = (prime_size_ + 63) / 64;
}

int NCryptSyncDiffHelm::Step1(const vector<uint64_t>& arr_in, vector<uint64_t>& arr_out, int step, NRandomGen& rg) {
	switch (step) {
	case 0:
	{
		if (!have_prime_) {
			p = RandomExtraPrime(prime_size_, rg);
		}

		do {
			g = RandomNumberGen(rg, prime_size_ + 10, false) % p;
		} while (PowMod(g, 2, p) == 1 || PowMod(g, (p - 1) / 2, p) == 1);

		a = RandomNumberGen(rg, prime_size_ + 10, false) % (p - 4) + 2;

		A = PowMod(g, a, p);

		if (g.Size() > prime_data_size_)
			return -1;
		if (p.Size() > prime_data_size_)
			return -1;
		if (A.Size() > prime_data_size_)
			return -1;
		size_t szg = g.Size(), szp = p.Size(), szA = A.Size();

		arr_out.resize(prime_data_size_ * 3);
		std::fill(arr_out.begin(), arr_out.end(), 0);

		for (size_t i = 0; i < szg; i++) {
			arr_out[i] = g.Data()[i];
		}
		for (size_t i = 0; i < szp; i++) {
			arr_out[i + prime_data_size_] = p.Data()[i];
		}
		for (size_t i = 0; i < szA; i++) {
			arr_out[i + prime_data_size_ * 2] = A.Data()[i];
		}

		return 0;
	}
	case 1:
	{
		vector<uint64_t> arr;

		if (arr_in.size() != prime_data_size_)
			return -1;
		arr.resize(prime_data_size_);
		for (size_t i = 0; i < prime_data_size_; i++) {
			arr[i] = arr_in[i];
		}
		B = bigint(arr, 1);

		K = PowMod(B, a, p);

		arr_out.resize(0);
		return 1;
	}
	default:
		return -1;
	}
}

int NCryptSyncDiffHelm::Step2(const vector<uint64_t>& arr_in, vector<uint64_t>& arr_out, int step, NRandomGen& rg) {
	switch (step) {
	case 0:
	{
		vector<uint64_t> arr;

		if (arr_in.size() != prime_data_size_ * 3)
			return -1;
		arr.resize(prime_data_size_);

		for (size_t i = 0; i < prime_data_size_; i++) {
			arr[i] = arr_in[i];
		}
		g = bigint(arr, 1);

		for (size_t i = 0; i < prime_data_size_; i++) {
			arr[i] = arr_in[i + prime_data_size_];
		}
		p = bigint(arr, 1);

		for (size_t i = 0; i < prime_data_size_; i++) {
			arr[i] = arr_in[i + prime_data_size_ * 2];
		}
		A = bigint(arr, 1);

		b = RandomNumberGen(rg, prime_size_ + 10, false) % (p - 4) + 2;

		K = PowMod(A, b, p);
		B = PowMod(g, b, p);

		arr_out.resize(prime_data_size_);
		for (size_t i = 0; i < B.Size(); i++) {
			arr_out[i] = B.Data()[i];
		}

		return 1;
	}
	default:
		return -1;
	}
}

size_t NCryptSyncDiffHelm::StepSize(int type, int step) {
	switch (type) {
	case 1:
		switch (step) {
		case 0:
			return 0;
		case 1:
			return prime_data_size_;
		default:
			return -1;
		}
	case 2:
		switch (step) {
		case 0:
			return prime_data_size_ * 3;
		default:
			return -1;
		}
	default:
		return -1;
	}
}

vector<uint64_t> NCryptSyncDiffHelm::GetResult() {
	return vector<uint64_t>(K.Data(), K.Data() + K.Size());
}

size_t NCryptSyncDiffHelm::RandomBitsCount() {
	return prime_size_ - 2;
}
