#pragma once

#include "NCryptBase.h"

class NCryptSyncDiffHelm final : public NCryptSync {
	bigint p, g, A, B, K, a, b;
	size_t prime_size_ = 1026;
	bool have_prime_ = false;
	size_t prime_data_size_ = (1026 + 63) / 64;
public:
	//Set prime. Recommend generate your own prime. 3rd party number can contains a backdoor.
	NCRYPT_DLL_API void SetPrime(const bigint& prime);
	//Set size of prime. Need same on client and server.
	NCRYPT_DLL_API void SetPrimeSize(int sz);
	//Step for person 1
	NCRYPT_DLL_API int Step1(const vector<uint64_t>& arr_in, vector<uint64_t>& arr_out, int step, NRandomGen& rg) override;
	//Step for person 2
	NCRYPT_DLL_API int Step2(const vector<uint64_t>& arr_in, vector<uint64_t>& arr_out, int step, NRandomGen& rg) override;
	//Step input size.
	NCRYPT_DLL_API size_t StepSize(int type, int step) override;
	//Get shared secret number
	NCRYPT_DLL_API vector<uint64_t> GetResult() override;
	//Count of random bits in secret num
	NCRYPT_DLL_API size_t RandomBitsCount() override;
};

