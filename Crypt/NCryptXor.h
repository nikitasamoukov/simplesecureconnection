#pragma once

#include "NCryptBase.h"

class NCryptXor final : public NCryptSymmetric {
	std::vector<uint64_t> key_{ 1 };
public:

	bool SetKey(const std::vector<uint64_t>& new_key) override {
		if (!new_key.empty()) {
			key_ = new_key;
		}
		return false;
	}

	// Key size in bytes
	uint64_t GetKeySize() override {
		return key_.size() * 8;
	};

	uint64_t GetDataScale() override {
		return 8;
	}

	// Data scale in bytes
	bool Encode(void* src, void* dst, uint64_t sz) override {
		if (sz % 8 != 0) {
			return false;
		}
		auto isrc = reinterpret_cast<uint64_t*>(src);
		auto idest = reinterpret_cast<uint64_t*>(dst);
		for (size_t i = 0; i < sz / 8; i++) {
			idest[i] = isrc[i] ^ key_[i % key_.size()];
		}
		return true;
	}

	bool Decode(void* src, void* dst, uint64_t sz) override {
		return Encode(src, dst, sz);
	}
};