#include "bigint.h"
#include <sstream>
#include <iostream>
using namespace std;

// Assembler commands need here
inline uint64_t AddCarry(uint64_t a, uint64_t b, bool& c) {
	uint64_t res = a + b + c;
	c = c ? res <= b : res < b;
	return res;
}

inline uint64_t SubCarry(uint64_t a, uint64_t b, bool& c) {
	uint64_t res = a - b - c;
	c = c ? res >= a : res > a;
	return res;
}

inline uint64_t RrCarry(uint64_t a, bool& c) {
	uint64_t res = (a >> 1);
	if (c)
		res |= 0x8000000000000000;
	c = a % 2 == 1;
	return res;
}

inline void RlBig(uint64_t a, int sdv, uint64_t& res1, uint64_t& res2) {
	res1 = a << (sdv);
	res2 = a >> (64 - sdv);
}

inline void RrBig(uint64_t a, int sdv, uint64_t& res1, uint64_t& res2) {
	res1 = a << (64 - sdv);
	res2 = a >> (sdv);
}

inline void MulBig(uint64_t a, uint64_t b, uint64_t& res1, uint64_t& res2) {
	uint64_t a0 = a & 0x00000000FFFFFFFF;
	uint64_t a1 = a >> 32;
	uint64_t b0 = b & 0x00000000FFFFFFFF;
	uint64_t b1 = b >> 32;

	uint64_t r00 = a0 * b0;
	uint64_t r01 = a0 * b1;
	uint64_t r10 = a1 * b0;
	uint64_t r11 = a1 * b1;

	res1 = r00;
	res2 = r11;

	bool c = false;
	uint64_t mid = AddCarry(r01, r10, c);
	if (c)
		res2 += (uint64_t)1 << 32;
	c = false;
	res1 = AddCarry(res1, mid << 32, c);
	if (c)
		res2 += 1;
	res2 += mid >> 32;
}

bigint::bigint() {
	sign_ = 1;
}

bigint::bigint(int64_t a) {
	if (a < 0) {
		sign_ = -1;
		data_.resize(1);
		data_[0] = -a;
	} else
		if (a > 0) {
			sign_ = 1;
			data_.resize(1);
			data_[0] = a;
		} else {
			sign_ = 1;
			data_.resize(0);
		}
}

bigint::bigint(int a) {
	if (a < 0) {
		sign_ = -1;
		data_.resize(1);
		data_[0] = -a;
	} else
		if (a > 0) {
			sign_ = 1;
			data_.resize(1);
			data_[0] = a;
		} else {
			sign_ = 1;
			data_.resize(0);
		}
}

bigint::bigint(uint64_t a) {
	sign_ = 1;
	data_.resize(1);
	data_[0] = a;
	Fit();
}

bigint::bigint(const bigint& a) {
	sign_ = a.sign_;
	data_ = a.data_;
}

bigint::bigint(bigint&& a) noexcept {
	sign_ = a.sign_;
	std::swap(data_, a.data_);
}

bool IsDecDigit(char digit) {
	return digit >= '0' && digit <= '9';
}

uint64_t DecDigitToNumber(char digit) {
	return uint64_t(digit) - '0';
}

bool IsHexDigit(char digit) {
	return digit >= '0' && digit <= '9' || digit >= 'a' && digit <= 'f' || digit >= 'A' && digit <= 'F';
}

uint64_t HexDigitToNumber(char digit) {
	if (digit >= '0' && digit <= '9') {
		return uint64_t(digit) - '0';
	}
	if (digit >= 'a' && digit <= 'f') {
		return uint64_t(digit) + 10ull - 'a';
	}
	if (digit >= 'A' && digit <= 'F') {
		return uint64_t(digit) + 10ull - 'A';
	}
	throw std::invalid_argument("Conversion string to bigint incorrect");
}

bigint::bigint(uint64_t* data, size_t size, int sign) :
	data_(data, data + size),
	sign_(sign < 0 ? -1 : 1) {}

bigint::bigint(const string& s) {
	sign_ = 1;

	int sign = 1;

	// Empty string
	if (s.empty()) {
		data_.clear();
		return;
	}

	// One digit string
	if (s.size() == 1) {
		if (IsDecDigit(s[0])) {
			data_.resize(1);
			data_[0] = DecDigitToNumber(s[0]);
		} else {
			throw std::invalid_argument("Conversion string to bigint incorrect");
		}
		return;
	}

	size_t offset = 0;
	if (s[0] == '-') {
		sign = -1;
		offset = 1;
	}
	if (s[0] == '+') {
		offset = 1;
	}

	// Signed one digit string
	if (s.size() == 2 && offset == 1) {
		if (IsDecDigit(s[1])) {
			data_.resize(1);
			data_[0] = DecDigitToNumber(s[1]);
		} else {
			throw std::invalid_argument("Conversion string to bigint incorrect");
		}
		sign_ = sign;
		return;
	}

	// Hex number parse
	if (s[offset] == '0' && (s[offset + 1] == 'x' || s[offset + 1] == 'X')) {

		for (uint64_t i = offset + 2; i < s.size(); i++) {
			if (!IsHexDigit(s[i])) {
				throw std::invalid_argument("Conversion string to bigint incorrect");
			}
			*this = (*this << 4) + HexDigitToNumber(s[i]);
		}
		sign_ = sign;
		return;
	}

	// Dec number parse
	for (uint64_t i = offset; i < s.size(); i++) {
		if (!IsDecDigit(s[i])) {
			throw std::invalid_argument("Conversion string to bigint incorrect");
		}
		*this = *this * 10 + DecDigitToNumber(s[i]);
	}
	sign_ = sign;
}

bigint::~bigint() {}

uint64_t bigint::GetUint64() const {
	if (data_.empty()) {
		return 0;
	}
	return data_[0];
}

int bigint::GetSign() const {
	return sign_;
}

bigint& bigint::operator=(const bigint& a) {
	if (this == &a)
		return *this;

	sign_ = a.sign_;
	data_ = a.data_;

	return *this;
}

bigint& bigint::operator=(bigint&& a) noexcept {
	sign_ = a.sign_;
	data_ = std::move(a.data_);

	return *this;
}

bigint operator+(const bigint& a, const bigint& b) {
	bigint res;
	if (a.sign_ == b.sign_) {
		res.data_.resize(std::max(a.data_.size(), b.data_.size()) + 1, 0);
		for (size_t i = 0; i < a.data_.size(); i++) {
			res.data_[i] = a.data_[i];
		}
		res.sign_ = a.sign_;
		res.OperationAdd(b);
	} else {
		switch (a.OperationCompare(b)) {
		case 1:
			res.data_.resize(std::max(a.data_.size(), b.data_.size()), 0);
			res.sign_ = a.sign_;
			for (size_t i = 0; i < a.data_.size(); i++) {
				res.data_[i] = a.data_[i];
			}
			res.OperationSub(b);
			break;
		case -1:
			res.data_.resize(std::max(a.data_.size(), b.data_.size()), 0);
			res.sign_ = b.sign_;
			for (size_t i = 0; i < b.data_.size(); i++) {
				res.data_[i] = b.data_[i];
			}
			res.OperationSub(a);
			break;
		case 0: // a + b = 0
			break;
		default:
			throw std::runtime_error("Unreachable code");
		}
	}

	res.Fit();
	return res;
}

bigint operator-(const bigint& a, const bigint& b) {
	bigint res;
	if (a.sign_ != b.sign_) {
		res.data_.resize(std::max(a.data_.size(), b.data_.size()) + 1, 0);
		for (size_t i = 0; i < a.data_.size(); i++) {
			res.data_[i] = a.data_[i];
		}
		res.sign_ = a.sign_;
		res.OperationAdd(b);
	} else {
		switch (a.OperationCompare(b)) {
		case 1:
			res.data_.resize(std::max(a.data_.size(), b.data_.size()), 0);
			res.sign_ = a.sign_;
			for (size_t i = 0; i < a.data_.size(); i++) {
				res.data_[i] = a.data_[i];
			}
			res.OperationSub(b);
			break;
		case -1:
			res.data_.resize(std::max(a.data_.size(), b.data_.size()), 0);
			res.sign_ = -b.sign_;
			for (size_t i = 0; i < b.data_.size(); i++) {
				res.data_[i] = b.data_[i];
			}
			res.OperationSub(a);
			break;
		default:
			throw std::runtime_error("Unreachable code");
		}
	}
	res.Fit();
	return res;
}

bigint operator*(const bigint& a, const bigint& b) {
	bigint res;
	auto new_size = a.data_.size() + b.data_.size();
	res.data_.resize(new_size);
	res.sign_ = a.sign_ * b.sign_;
	for (size_t i = 0; i < a.data_.size(); i++) {
		bigint p;
		p.data_.resize(new_size);

		for (size_t r = 0; r < b.data_.size(); r += 2) {
			MulBig(a.data_[i], b.data_[r], p.data_[i + r], p.data_[i + r + 1]);
		}
		res.OperationAdd(p);
		p.data_.resize(0);
		p.data_.resize(new_size);
		for (size_t r = 1; r < b.data_.size(); r += 2) {
			MulBig(a.data_[i], b.data_[r], p.data_[i + r], p.data_[i + r + 1]);
		}
		res.OperationAdd(p);
	}
	res.Fit();
	return res;
}

int GetRightBitNum(uint64_t a) {
	int res = 0;
	if (a & 0xFFFFFFFF00000000) { res += 32; a >>= 32; }
	if (a & 0x00000000FFFF0000) { res += 16; a >>= 16; }
	if (a & 0x000000000000FF00) { res += 8; a >>= 8; }
	if (a & 0x00000000000000F0) { res += 4; a >>= 4; }
	if (a & 0x000000000000000C) { res += 2; a >>= 2; }
	if (a & 0x0000000000000002) { res += 1; }
	return res;
}

int GetLeftBitNum(uint64_t a) {
	int res = 0;
	if (!(a & 0x00000000FFFFFFFF)) { res += 32; a >>= 32; }
	if (!(a & 0x000000000000FFFF)) { res += 16; a >>= 16; }
	if (!(a & 0x00000000000000FF)) { res += 8; a >>= 8; }
	if (!(a & 0x000000000000000F)) { res += 4; a >>= 4; }
	if (!(a & 0x0000000000000003)) { res += 2; a >>= 2; }
	if (!(a & 0x0000000000000001)) { res += 1; }
	return res;
}

bigint operator/(const bigint& a, const bigint& b) {
	bigint resdiv;
	int numdiff = a.FirstBitPos() - b.FirstBitPos();
	if (numdiff < 0)
		return resdiv;
	bigint bmovv = b << numdiff;
	bigint resmod = a;
	resdiv.data_.resize(a.data_.size(), 0);
	for (int i = numdiff; i >= 0; i--) {
		if (resmod.OperationCompare(bmovv) != -1) {
			resmod.OperationSub(bmovv);
			resdiv.data_[i / 64] |= (uint64_t)1 << (i % 64);
		}
		bmovv.OperationRr();
	}

	resdiv.Fit();
	resmod.Fit();
	resdiv.sign_ = a.sign_ * b.sign_;
	return resdiv;
}

bigint operator%(const bigint& a, const bigint& b) {
	bigint resdiv;
	bigint resmod = a;
	int numdiff = a.FirstBitPos() - b.FirstBitPos();
	if (numdiff < 0)
		return resmod;
	bigint bmovv = b << numdiff;
	resdiv.data_.resize(a.data_.size(), 0);
	for (int i = numdiff; i >= 0; i--) {
		if (resmod.OperationCompare(bmovv) != -1) {
			resmod.OperationSub(bmovv);
			resdiv.data_[i / 64] |= (uint64_t)1 << (i % 64);
		}
		bmovv.OperationRr();
	}

	resdiv.Fit();
	resmod.Fit();
	resmod.sign_ = a.sign_ * b.sign_;
	return resmod;
}

bool operator>(const bigint& a, const bigint& b) {
	if (a.sign_ == -1 && b.sign_ == -1)
		return b.OperationCompare(a) == 1;
	if (a.sign_ == 1 && b.sign_ == 1)
		return a.OperationCompare(b) == 1;
	if (a.sign_ == -1 && b.sign_ == 1)
		return false;

	return (a != 0) || (b != 0);
}

bool operator<(const bigint& a, const bigint& b) {
	return b > a;
}

bool operator==(const bigint& a, const bigint& b) {
	if (a.sign_ == b.sign_) {
		for (size_t i = 0; i < min(a.data_.size(), b.data_.size()); i++) {
			if (a.data_[i] != b.data_[i]) {
				return false;
			}
		}
		if (a.data_.size() > b.data_.size()) {
			for (size_t i = min(a.data_.size(), b.data_.size()); i < max(a.data_.size(), b.data_.size()); i++) {
				if (a.data_[i] != 0) {
					return false;
				}
			}
		}
		if (b.data_.size() > a.data_.size()) {
			for (size_t i = min(a.data_.size(), b.data_.size()); i < max(a.data_.size(), b.data_.size()); i++) {
				if (b.data_[i] != 0) {
					return false;
				}
			}
		}
		return true;
	}

	for (const auto& e : a.data_)
		if (e != 0)
			return false;
	for (const auto& e : b.data_)
		if (e != 0)
			return false;
	return true;
}

bool operator!=(const bigint& a, const bigint& b) {
	return !(a == b);
}

bigint operator<<(const bigint& a, int b) {
	bigint res;

	size_t sddiv = b / 64;
	int sdmod = b % 64;

	res.data_.resize(a.data_.size() + 1 + sddiv, 0);
	res.sign_ = a.sign_;

	if (sdmod != 0)
		for (size_t i = 0; i < a.data_.size(); i++) {
			uint64_t p1, p2;
			RlBig(a.data_[i], sdmod, p1, p2);
			res.data_[i + sddiv] |= p1;
			res.data_[i + sddiv + 1] |= p2;
		} else
			for (size_t i = 0; i < a.data_.size(); i++) {
				res.data_[i + sddiv] |= a.data_[i];
			}

		res.Fit();
		return res;
}

bigint operator>>(const bigint& a, int b) {
	bigint res;

	int sddiv = b / 64;
	int sdmod = b % 64;

	res.data_.resize(a.data_.size(), 0);
	res.sign_ = a.sign_;

	if (sdmod != 0)
		for (size_t i = a.data_.size(); i-- > 0;) {
			uint64_t p1, p2;
			RrBig(a.data_[i], sdmod, p1, p2);
			if (i - sddiv == 0) {
				res.data_[i - sddiv] |= p2;
				break;
			}
			res.data_[i - sddiv - 1] |= p1;
			res.data_[i - sddiv] |= p2;
		} else
			for (size_t i = a.data_.size(); i-- > 0;) {
				res.data_[i - sddiv] |= a.data_[i];
			}


		res.Fit();
		return res;
}

std::ostream& operator<<(std::ostream& out, const bigint& a) {
	if (a.sign_ == -1)
		out << '-';
	for (size_t i = a.data_.size(); i-- > 0;) {
		out << std::setfill('0') << std::setw(16) << std::hex << a.data_[i];
	}
	out << std::dec;
	return out;
}

bigint PowMod(const bigint& a, const bigint& b, const bigint& c) {

	bigint res(1);

	for (int i = b.FirstBitPos(); i >= 0; i--) {
		res = (res * res);
		res = res % c;
		if (b.data_[i / 64] & ((uint64_t)1 << (i % 64))) {
			res = (res * a) % c;
		}
	}

	return res;
}

bool TestPrime(const bigint& num, int rounds) {
	int res = 1;
	bigint b = num - 1;
	int s = b.LastBitPos();
	bigint t = b >> s;
	for (int i = 0; i < rounds && res; i++) {
		cout<<"+";
		bigint a, x;
		a.data_.resize(num.data_.size() + 1);
		for (auto& e : a.data_)
			e = rand() + ((uint64_t)rand() << 16) + ((uint64_t)rand() << 32) + ((uint64_t)rand() << 48);
		a = (a % (num - 3)) + 2;

		x = PowMod(a, t, num);

		if (x != 1 && x != num - 1) {
			int f = 0;
			for (int r = 0; r < s - 1 && f == 0; r++) {
				x = x * x % num;
				if (x == 1)
					res = 0;
				if (x == num - 1)
					f = 1;
			}
			if (f == 0)
				res = 0;

		}
	}

	return res;
}

bigint sqrt(const bigint& num) {
	bigint res = num;
	bigint div = num;
	while (true) {
		div = (num / div + div) / 2;
		if (res.OperationCompare(div))
			res = div;
		else
			return res;
	}
}

bigint NextPrime(const bigint& num) {
	bigint p1 = num + 1;

	constexpr int test_primes_count = 100;
	const static int osn[test_primes_count] = { 2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71
		,73,79,83,89,97,101,103,107,109,113,127,131,137,139,149,151,157,163,167,173
		,179,181,191,193,197,199,211,223,227,229,233,239,241,251,257,263,269,271,277,281
		,283,293,307,311,313,317,331,337,347,349,353,359,367,373,379,383,389,397,401,409
		,419,421,431,433,439,443,449,457,461,463,467,479,487,491,499,503,509,521,523,541 };

	int val[test_primes_count];
	for (int i = 0; i < test_primes_count; i++) {
		val[i] = static_cast<int>((p1 % osn[i]).GetUint64());
		if (num < osn[i])
			return osn[i];
	}

	while (true) {
		int inc = 0;

		int t1 = 0;
		while (!t1) {
			t1 = 1;
			inc++;
			for (int i = 0; i < test_primes_count; i++) {
				val[i] = (val[i] + 1) % osn[i];
				if (val[i] == 0)
					t1 = 0;
			}
		}
		p1 = p1 + inc;
		if (TestPrime(p1))
			return p1;
	}
}

bigint NextPrimeEx(const bigint& num) {

	if (num < 2048)
		throw;

	bigint p1 = num + 1;
	if (p1 % 2 == 0)
		p1 = p1 + 1;
	bigint p2 = (p1 - 1) / 2;

	constexpr int test_primes_count = 100;
	const static int osn[test_primes_count] = { 2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71
		,73,79,83,89,97,101,103,107,109,113,127,131,137,139,149,151,157,163,167,173
		,179,181,191,193,197,199,211,223,227,229,233,239,241,251,257,263,269,271,277,281
		,283,293,307,311,313,317,331,337,347,349,353,359,367,373,379,383,389,397,401,409
		,419,421,431,433,439,443,449,457,461,463,467,479,487,491,499,503,509,521,523,541 };

	int val1[test_primes_count], val2[test_primes_count];
	for (int i = 0; i < test_primes_count; i++) {
		val1[i] = static_cast<int>((p1 % osn[i]).GetUint64());
		val2[i] = static_cast<int>((p2 % osn[i]).GetUint64());
	}

	while (true) {
		int inc = 0;

		int t1 = 0;
		while (!t1) {
			t1 = 2;
			inc++;
			for (int i = 0; i < test_primes_count; i++) {
				val1[i] = (val1[i] + 2) % osn[i];
				if (val1[i] == 0)
					t1 = 0;
			}
			for (int i = 0; i < test_primes_count; i++) {
				val2[i] = (val2[i] + 1) % osn[i];
				if (val2[i] == 0)
					t1 = 0;
			}
		}
		p1 = p1 + inc + inc;
		p2 = p2 + inc;

		int tt = 1;

		for (int i = 0; i < 100 && tt; i++) {
			if (!TestPrime(p1, 1))
				tt = 0;
			if (tt)
				if (!TestPrime(p2, 1))
					tt = 0;
		}
		if (tt) {
			return p1;
		}
		cout<<"-"<<endl;
	}
}

string bigint::ToDec() const {
	bigint divc = 10000000000000000000uL;
	bigint a = *this;

	vector<char> res(20 * a.data_.size() + 1, '0');

	int offset = 0;
	while (a != 0) {
		uint64_t part = (a % divc).GetUint64();
		string s = std::to_string(part);
		for (size_t i = 0; i < s.size(); i++) {
			res[offset + (s.size() - 1 - i)] = s[i];
		}

		offset += 19;
		a = a / divc;
	}

	size_t num_start_pos = res.size() - 1;

	while (num_start_pos > 0 && res[num_start_pos] == '0') {
		num_start_pos--;
	}
	string res_string;
	res_string.reserve(num_start_pos + 2);

	if (sign_ == -1 && *this != 0) {
		res_string += '-';
	}
	for (; num_start_pos < res.size(); num_start_pos--) {
		res_string += res[num_start_pos];
	}

	return res_string;
}

string bigint::ToHex() const {
	stringstream stream;
	if (sign_ == -1)
		stream << '-';
	stream << "0x";

	for (size_t i = data_.size(); i-- > 0;) {
		stream << std::setfill('0') << std::setw(16) << std::hex << data_[i];
	}
	stream << std::dec;
	return stream.str();
}

void bigint::OperationAdd(const bigint& a) {
	bool c = false;
	for (size_t i = 0; i < a.data_.size(); i++) {
		data_[i] = AddCarry(data_[i], a.data_[i], c);
	}
	size_t i = a.data_.size();
	while (c) {
		data_[i] = AddCarry(data_[i], 0, c);
		i++;
	}
}

void bigint::OperationSub(const bigint& a) {
	bool c = false;
	size_t sub_count = min(a.data_.size(), data_.size());
	for (size_t i = 0; i < sub_count; i++) {
		data_[i] = SubCarry(data_[i], a.data_[i], c);
	}
	size_t i = a.data_.size();
	while (c) {
		data_[i] = SubCarry(data_[i], 0, c);
		i++;
	}
}

void bigint::OperationRr() {
	bool c = false;
	for (size_t i = data_.size(); i-- > 0;) {
		data_[i] = RrCarry(data_[i], c);
	}
}

int bigint::OperationCompare(const bigint& a) const {
	size_t n = data_.size();

	if (data_.size() > a.data_.size()) {
		for (size_t i = a.data_.size(); i < data_.size(); i++) {
			if (data_[i] != 0)
				return 1;
		}
		n = a.data_.size();
	}

	if (data_.size() < a.data_.size()) {
		for (size_t i = data_.size(); i < a.data_.size(); i++) {
			if (a.data_[i] != 0)
				return -1;
		}
		n = data_.size();
	}

	for (size_t i = n; i-- > 0;) {
		if (data_[i] > a.data_[i])
			return 1;
		if (data_[i] < a.data_[i])
			return -1;
	}

	return 0;
}

int bigint::FirstBitPos() const {
	for (size_t i = data_.size(); i-- > 0;) {
		if (data_[i] != 0)
			return static_cast<int>(GetRightBitNum(data_[i]) + i * 64);
	}
	return -1;
}

int bigint::LastBitPos() const {
	for (size_t i = 0; i < data_.size(); i++) {
		if (data_[i] != 0)
			return static_cast<int>(GetLeftBitNum(data_[i]) + i * 64);
	}
	return -1;
}

void bigint::Fit() {
	for (size_t i = data_.size(); i-- > 0;) {
		if (data_[i] == 0)
			data_.resize(i);
		else
			return;
	}
	data_.clear();
}

uint64_t* bigint::Data() {
	return data_.data();
}

size_t bigint::Size() const {
	return data_.size();
}
