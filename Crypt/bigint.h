#pragma once

#include <cstdint>
#include <string>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <algorithm>

#include "NCryptDefines.h"

using std::vector;
using std::string;
using std::min;
using std::max;

class bigint final {
	vector<uint64_t> data_;
	int sign_;
	void OperationAdd(const bigint&);
	void OperationSub(const bigint&);
	void OperationRr();
	int OperationCompare(const bigint&)const;
	void Fit();
public:
	NCRYPT_DLL_API bigint();
	NCRYPT_DLL_API bigint(int64_t);
	NCRYPT_DLL_API bigint(int);
	NCRYPT_DLL_API bigint(uint64_t);
	NCRYPT_DLL_API bigint(const bigint&);
	NCRYPT_DLL_API bigint(bigint&&) noexcept;
	explicit bigint(vector<uint64_t> data, int sign = 1) : bigint(data.data(), data.size(), sign) {}
	NCRYPT_DLL_API bigint(uint64_t* data, size_t size, int sign = 1);
	NCRYPT_DLL_API bigint(const string&);
	NCRYPT_DLL_API ~bigint();
	NCRYPT_DLL_API uint64_t GetUint64() const;
	NCRYPT_DLL_API int GetSign() const;
	NCRYPT_DLL_API int FirstBitPos() const;
	NCRYPT_DLL_API int LastBitPos() const;
	NCRYPT_DLL_API bigint& operator= (const bigint&);
	NCRYPT_DLL_API bigint& operator= (bigint&&) noexcept;
	NCRYPT_DLL_API friend bigint operator+ (const bigint&, const bigint&);
	NCRYPT_DLL_API friend bigint operator- (const bigint&, const bigint&);
	NCRYPT_DLL_API friend bigint operator* (const bigint&, const bigint&);
	NCRYPT_DLL_API friend bigint operator/ (const bigint&, const bigint&);
	NCRYPT_DLL_API friend bigint operator% (const bigint&, const bigint&);
	NCRYPT_DLL_API friend bool operator> (const bigint&, const bigint&);
	NCRYPT_DLL_API friend bool operator< (const bigint&, const bigint&);
	NCRYPT_DLL_API friend bool operator== (const bigint&, const bigint&);
	NCRYPT_DLL_API friend bool operator!= (const bigint&, const bigint&);
	NCRYPT_DLL_API friend bigint operator<< (const bigint&, int);
	NCRYPT_DLL_API friend bigint operator>> (const bigint&, int);
	NCRYPT_DLL_API friend std::ostream& operator<< (std::ostream&, const bigint&);
	NCRYPT_DLL_API friend bigint PowMod(const bigint&, const bigint&, const bigint&);
	NCRYPT_DLL_API friend bool TestPrime(const bigint& num, int rounds);
	NCRYPT_DLL_API friend bigint sqrt(const bigint& num);
	NCRYPT_DLL_API string ToDec() const;
	NCRYPT_DLL_API string ToHex() const;
	NCRYPT_DLL_API uint64_t* Data();
	NCRYPT_DLL_API size_t Size() const;
};

NCRYPT_DLL_API bool TestPrime(const bigint& num, int rounds = 100);
NCRYPT_DLL_API bigint NextPrime(const bigint& num);
NCRYPT_DLL_API bigint NextPrimeEx(const bigint& num);
