# SimpleSecureConnection GitLab

## Description

Simple lib to create secure connection. Created to be small and easy to read. 

Work only on Windows because:
- WINAPI Random
- WinSock
- Visual Studio solution

This my old project. I will be updating it.

## Parts

This library have 2 parts: Cryptography and Mathematics.

## Cryptography

Cryptography based on:
- Random generator
- Symmetric cryptography
- Asymmetric cryptography
- Diffie–Hellman key exchange
- Cryptographic hash
- Cipher Block Chaining(CBC) for channel data

Channel used for creating connecting between server and client.
It send binary packets with size < 2 GB.

## Mathematics

Cryptography need big integer numbers.
For operating, big numbers creates a bigint class.
It have:

- Basic conversions from string and to string, hex and dec.
- Basic operations: '+', '-', '*', '/', '==', '<', sqrt
- a = (b ^ c) % d
- Miller–Rabin primality test

Its work slow.

## Example

For secure connection need a N for Diffie–Hellman:
N is prime,
(N-1)/2 is prime.
For secure connect need 2048 bits in N.

For server auth need generate public and private keys with 2048 bits.




    bool TestClient(const vector<uint64_t>& public_key, vector<uint8_t>& data) {
    	MtPrint("Client: Init");
    
    	shared_ptr<NCryptAuthRsa> auth(new NCryptAuthRsa());
    	auth->SetPublicKey(public_key);
    
    	shared_ptr<NCryptSyncDiffHelm> sync(new NCryptSyncDiffHelm());
    	sync->SetPrimeSize(500);
    	sync->SetPrime(bigint("0x000d80b7ee5b8da724e173bc283eef9a010a85968b81f4fc998e9a848b349f9939d2591a655bd12a529e142e8c961bdbdf775d580ab1f1d953a21e5123c4fcfb"));
    	
    	NCryptStr str;
    
    	str.auth = auth;
    	str.connect = make_shared<NCryptConnectionWs>();
    	str.hash = make_shared<NCryptHashSha3>();
    	str.rg = make_shared<NRandomGenSeed>(data.size());
    	str.symm = make_shared<NCryptSymIdea>();
    	str.sync = sync;
    
    	NCryptChannel channel(str);
    
    	MtPrint("Client: Connect");
    	{
    		bool success = channel.Connect("127.0.0.1", 22);
    		if (!success) {
    			MtPrint("Client: Connect error");
    			return false;
    		}
    	}
    
    	MtPrint("Client: Sync key");
    	{
    		bool success = channel.SyncKey();
    		if (!success) {
    			MtPrint("Client: SyncKey error");
    			return false;
    		}
    	}
    	MtPrint("Client: Sync key : " + bigint(sync->GetResult(), 1).ToHex());
    
    	MtPrint("Client: Send data");
    	{
    		bool success = channel.SendData(data.data(), data.size());
    		if (!success) {
    			MtPrint("Client: Send data error");
    			return false;
    		}
    	}
    
    	MtPrint("Client: Test success");
    	return true;
    }
    
    NCryptStr CreateServerCryptStr(const vector<uint64_t>& private_key) {
    	shared_ptr<NCryptAuthRsa> auth(new NCryptAuthRsa());
    	auth->SetPrivateKey(private_key);
    
    	shared_ptr<NCryptSyncDiffHelm> sync(new NCryptSyncDiffHelm());
    	sync->SetPrimeSize(500);
    
    	NCryptStr str;
    
    	str.auth = auth;
    	str.connect = make_shared<NCryptConnectionWs>();
    	str.hash = make_shared<NCryptHashSha3>();
    	str.rg = make_shared<NRandomGenSeed>();
    	str.symm = make_shared<NCryptSymIdea>();
    	str.sync = sync;
    
    	return str;
    }
    
    bool TestServer(const vector<uint64_t>& private_key, size_t count_echo) {
    	MtPrint("Server: Init");
    
    	NCryptConnectionWs listener;
    	listener.ListenClients(22);
    
    	auto connection = shared_ptr<NCryptConnection>(listener.AcceptClient(10));
    
    	{
    		// This code can be moved to other thread
    		NCryptStr str = CreateServerCryptStr(private_key);
    		NCryptChannel channel(str);
    
    		MtPrint("Server: Listen");
    		{
    			bool success = channel.ServerConnect(connection);
    			if (success == false) {
    				return false;
    			}
    		}
    
    		MtPrint("Server: Sync key");
    		{
    			bool success = channel.SyncKey();
    			if (!success) {
    				return false;
    			}
    		}
    		MtPrint("Server: Sync key : " + bigint(str.sync->GetResult(), 1).ToHex());
    
    		vector<uint8_t> data_from_client;
    
    		MtPrint("Server: Recv data");
    
    		{
    			bool success = channel.RecvData(data_from_client);
    			if (!success) {
    				return false;
    			}
    		}
    	}
    
    	MtPrint("Server: Test success");
    	return true;
    }


## Licensing

See the [LICENSE](LICENSE) file for licensing information as it pertains to files in this repository.
