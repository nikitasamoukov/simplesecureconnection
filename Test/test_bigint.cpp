#include "gtest/gtest.h"
#include "bigint.h"

using namespace std;

TEST(BigInt_UnitTestsRun, BigInt) {
	{
		bigint a;
	}
	SUCCEED();
}

TEST(BigInt_UnitTestsRun, BigIntInit) {
	{
		bigint a = 42;
	}
	{
		bigint a(42);
	}
	{
		bigint a{ 42 };
	}
	{
		bigint a = bigint(42);
	}
	{
		bigint a(bigint(42));
	}
	{
		bigint a{ bigint(42) };
	}
	{
		bigint a = { "42" };
	}
	{
		bigint a("42");
	}
	{
		bigint a{ "42" };
	}
	{
		bigint a = bigint("42");
	}
	{
		bigint a(bigint("42"));
	}
	{
		bigint a{ bigint("42") };
	}

	SUCCEED();
}
	
TEST(BigInt_UnitTestsRun, BigIntOperations)
{
	bigint a = 42;
	bigint b = -42;
	{
		bigint c = a + b;
	}
	{
		bigint c = a - b;
	}
	{
		bigint c = a * b;
	}
	{
		bigint c = a / b;
	}
	{
		bigint c = a % b;
	}
	SUCCEED();
}

TEST(BigInt_UnitTestsCheck, BigIntInit) {
	{
		bigint a = 42;
		ASSERT_TRUE(a.ToDec() == "42");
	}
	{
		bigint a(42);
		ASSERT_TRUE(a.ToDec() == "42");
	}
	{
		bigint a{ 42 };
		ASSERT_TRUE(a.ToDec() == "42");
	}
	{
		bigint a = bigint(42);
		ASSERT_TRUE(a.ToDec() == "42");
	}
	{
		bigint a(bigint(42));
		ASSERT_TRUE(a.ToDec() == "42");
	}
	{
		bigint a{ bigint(42) };
		ASSERT_TRUE(a.ToDec() == "42");
	}
	{
		bigint a = { "42" };
		ASSERT_TRUE(a.ToDec() == "42");
	}
	{
		bigint a("42");
		ASSERT_TRUE(a.ToDec() == "42");
	}
	{
		bigint a{ "42" };
		ASSERT_TRUE(a.ToDec() == "42");
	}
	{
		bigint a = bigint("42");
		ASSERT_TRUE(a.ToDec() == "42");
	}
	{
		bigint a(bigint("42"));
		ASSERT_TRUE(a.ToDec() == "42");
	}
	{
		bigint a{ bigint("42") };
		ASSERT_TRUE(a.ToDec() == "42");
	}
}

TEST(BigInt_UnitTestsCheck, BigIntEquals)
{
	for (int number : {42, 15, -234124, 0, -1, 1}) {
		ASSERT_TRUE(bigint(number) == number);
	}
}

TEST(BigInt_IntegrationTestsCheck, Convert)
{
	auto arr_dec = {
	"1",
	"42000000000000000000000000000000000000000000000000000000000000000000000000000",
	"-4200000000000000000000000000000000000000001549837457612345678900000000000000000000000000000000000",
	"0",
	"123123123",
	"-232134",
	"-1"};

	for (auto& s : arr_dec) {
		ASSERT_TRUE(bigint(s).ToDec() == s);
	}

	auto arr_hex = { "0x42000000000000000000000000000000000000000000000000000000000000000000000000000",
	"-0x4200000000000000000000000000000000000000001549837457612345678900000000000000000000000000000000000",
	"0x0",
	"0x123123123",
	"-0x232134",
	"-0x1",
	"0x1" };

	for (auto& s : arr_hex) {
		string val_str = bigint(s).ToHex();
		bigint val1 = bigint(bigint(s).ToHex());
		bigint val2 = bigint(s);
		ASSERT_TRUE(val1 == val2);
	}
}

TEST(BigInt_IntegrationTestsCheck, PowModTest)
{
	{
		bigint a = bigint("0xffffffff4632352562345ffffffff");
		bigint b = bigint("0xffffffffffff4657462ffff");
		bigint c = bigint("0xffffffff46317b7ca864e6b02d81ba581ec609bbe8548b9d0001");
		ASSERT_TRUE(a * b == c);
	}
	{
		bigint a = bigint("123123");
		bigint b = bigint("123412");
		bigint c = bigint("123458");
		bigint d = bigint("58493");
		ASSERT_TRUE(PowMod(a, b, c) == d);
	}
}