#include "gtest/gtest.h"

#include "NCryptAuthRsa.h"
#include "NCryptHashSha3.h"
#include "NCryptSymIdea.h"
#include "NCryptSyncDiffHelm.h"
#include "NCryptConnectionWs.h"
#include "NCryptRandom.h"

#include <future>

#include "util.h"

using namespace std;


TEST(Crypt_UnitTestsRun, CreateConnection) {
	{
		NCryptConnectionWs conn;
	}
	SUCCEED();
}

TEST(Crypt_UnitTestsRun, AuthGenerate) {
	NCryptAuthRsa auth;
	vector<uint64_t> private_key, public_key;
	auth.Generate(64, private_key, public_key, N_BASIC_RG);
	ASSERT_FALSE(private_key.empty());
	ASSERT_FALSE(public_key.empty());
}

TEST(Crypt_UnitTestsRun, RandomNumber) {
	auto res = RandomPrime(100, N_BASIC_RG);
	ASSERT_TRUE(res % 3 != 0);
}

TEST(Crypt_UnitTestsCheck, Rsa) {
	NCryptAuthRsa auth;
	vector<uint64_t> private_key, public_key;
	ASSERT_TRUE(auth.Generate(64, private_key, public_key, N_BASIC_RG));

	ASSERT_TRUE(auth.SetPrivateKey(private_key));
	ASSERT_TRUE(auth.SetPublicKey(public_key));

	vector<uint64_t>data = ConvertVector8To64(GetDataSet(32));
	vector<uint64_t>encrypted_data;

	ASSERT_TRUE(auth.Crypt(data, encrypted_data));

	ASSERT_TRUE(auth.Test(encrypted_data, data));
}

TEST(Crypt_UnitTestsCheck, IDEA) {
	vector<uint8_t> source_data = GetDataSet();
	vector<uint8_t> encrypted_data(source_data.size());
	vector<uint8_t> decrypted_data(source_data.size());

	NCryptSymIdea idea;

	source_data.resize((source_data.size() + (idea.GetDataScale() - 1)) / idea.GetDataScale() * idea.GetDataScale());

	ASSERT_TRUE(idea.SetKey(vector<uint64_t>() = { 0x1234123a3e569854, 0xfa5ff4a014567215 }));

	ASSERT_TRUE(idea.Encode(source_data.data(), encrypted_data.data(), source_data.size()));
	ASSERT_TRUE(idea.Decode(encrypted_data.data(), decrypted_data.data(), source_data.size()));

	ASSERT_TRUE(source_data == decrypted_data);
}

TEST(Crypt_UnitTestsCheck, DiffHelm) {
	NCryptSyncDiffHelm sync_client, sync_server;
	vector<uint64_t> data_client_input;// data_server_output
	vector<uint64_t> data_server_input;// data_client_output

	sync_client.SetPrimeSize(64);
	sync_server.SetPrimeSize(64);

	ASSERT_TRUE(sync_client.Step1(data_client_input, data_server_input, 0, N_RG_SEED) == 0);

	ASSERT_TRUE(sync_server.Step2(data_server_input, data_client_input, 0, N_RG_SEED) == 1);

	ASSERT_TRUE(sync_client.Step1(data_client_input, data_server_input, 1, N_RG_SEED) == 1);

	vector<uint64_t> key_client = sync_client.GetResult();
	vector<uint64_t> key_server = sync_server.GetResult();
	ASSERT_TRUE(key_client.size() == key_server.size());
	for (uint64_t i = 0; i < key_client.size(); i++) {
		ASSERT_TRUE(key_client[i] == key_server[i]);
	}
}

TEST(Crypt_UnitTestsCheck, SHA3) {
	NCryptHashSha3 sha3;

	vector<pair<string, string>> hash_tests =
	{
		{"", "a69f73cca23a9ac5c8b567dc185a756e97c982164fe25859e0d1dcc1475c80a615b2123af1f5f94c11e3e9402c3ac558f500199d95b6d3e301758586281dcd26"},
		{"11111111", "39ff69aff5ac9e7d8be140acde27bfd6e7a71b54691796ab3ac0996c82893f46b7a1e46f3b5e380b373b0030dd097b04628990751d1c680159f29a48fde96fa9"},
		{"The quick brown fox jumps over the lazy dog","01dedd5de4ef14642445ba5f5b97c15e47b9ad931326e4b0727cd94cefc44fff23f07bf543139939b49128caf436dc1bdee54fcb24023a08d9403f9b4bf0d450"},
		{"The quick brown fox jumps over the lazy dog.", "18f4f4bd419603f95538837003d9d254c26c23765565162247483f65c50303597bc9ce4d289f21d1c2f1f458828e33dc442100331b35e7eb031b5d38ba6460f8"},
	};

	for (auto& hash_test : hash_tests) {
		vector<uint8_t> hash(sha3.HashSize());
		sha3.Hash(hash_test.first.c_str(), hash_test.first.size(), hash.data());
		ASSERT_TRUE(hash.size() * 2 == hash_test.second.size());
		for (size_t i = 0; i < hash.size(); i++) {
			auto hex_code = ByteToHex(reinterpret_cast<char*>(hash.data())[i]);
			if (hash_test.second[i * 2] != hex_code.first || hash_test.second[i * 2 + 1] != hex_code.second) {
				cout << hash_test.first << ":" << hash_test.second << endl;
				FAIL();
			}
		}
	}
}

int main(int ac, char* av[]) {
	testing::InitGoogleTest(&ac, av);
	return RUN_ALL_TESTS();
}