#include "gtest/gtest.h"

#include "NCryptAuthRsa.h"
#include "NCryptHashSha3.h"
#include "NCryptSymIdea.h"
#include "NCryptSyncDiffHelm.h"
#include "NCryptConnectionWs.h"
#include "NCryptChannel.h"
#include "NCryptRandom.h"

#include <future>

#include "util.h"

using namespace std;

namespace {

bool TestClient(const vector<uint64_t>& public_key, vector<vector<uint8_t>>& data, shared_ptr<NCryptSymmetric> symm) {
	MtPrint("Client: Init");

	shared_ptr<NCryptAuthRsa> auth(new NCryptAuthRsa());
	auth->SetPublicKey(public_key);

	shared_ptr<NCryptSyncDiffHelm> sync(new NCryptSyncDiffHelm());
	sync->SetPrimeSize(500);
	sync->SetPrime(bigint("0x000d80b7ee5b8da724e173bc283eef9a010a85968b81f4fc998e9a848b349f9939d2591a655bd12a529e142e8c961bdbdf775d580ab1f1d953a21e5123c4fcfb"));

	NCryptStr str;

	str.auth = auth;
	str.connect = make_shared<NCryptConnectionWs>();
	str.hash = make_shared<NCryptHashSha3>();
	str.rg = make_shared<NRandomGenSeed>();
	str.symm = symm;
	str.sync = sync;

	NCryptChannel channel(str);

	MtPrint("Client: Connect");
	{
		bool success = channel.Connect("127.0.0.1", 22);
		if (!success) {
			return false;
		}
	}

	MtPrint("Client: Sync key");
	{
		bool success = channel.SyncKey();
		if (!success) {
			return false;
		}
	}
	MtPrint("Client: Sync key : " + bigint(sync->GetResult(), 1).ToHex());

	for (size_t i = 0; i < data.size(); i++) {
		auto& arr = data[i];
		MtPrint("Client: Send data " + to_string(i));
		{
			bool success = channel.SendData(arr.data(), arr.size());
			if (!success) {
				return false;
			}

		}
		{
			vector<uint8_t> data_processed_local = arr;
			for_each(data_processed_local.begin(), data_processed_local.end(), [](auto& e) {e -= 1; });
			vector<uint8_t> data_from_server;

			MtPrint("Client: Recv data " + to_string(i));

			bool success = channel.RecvData(data_from_server);
			if (!success) {
				return false;
			}

			if (data_from_server != data_processed_local) {
				return false;
			}
			MtPrint("Client: Data test ok " + to_string(i));
		}
	}

	MtPrint("Client: Test success");
	return true;
}

bool TestServer(const vector<uint64_t>& private_key, vector<vector<uint8_t>>& data, shared_ptr<NCryptSymmetric> symm) {
	MtPrint("Server: Init");

	shared_ptr<NCryptAuthRsa> auth(new NCryptAuthRsa());
	auth->SetPrivateKey(private_key);

	shared_ptr<NCryptSyncDiffHelm> sync(new NCryptSyncDiffHelm());
	sync->SetPrimeSize(500);

	NCryptStr str;

	str.auth = auth;
	str.connect = make_shared<NCryptConnectionWs>();
	str.hash = make_shared<NCryptHashSha3>();
	str.rg = make_shared<NRandomGenSeed>();
	str.symm = symm;
	str.sync = sync;

	NCryptChannel channel(str);

	NCryptConnectionWs listener;
	listener.ListenClients(22);
	auto connection = shared_ptr<NCryptConnection>(listener.AcceptClient(1));

	MtPrint("Server: Listen");
	{
		bool success = channel.ServerConnect(connection);
		if (success == false) {
			return false;
		}
	}

	MtPrint("Server: Sync key");
	{
		bool success = channel.SyncKey();
		if (!success) {
			return false;
		}
	}
	MtPrint("Server: Sync key : " + bigint(sync->GetResult(), 1).ToHex());

	vector<uint8_t> data_from_client;

	for (size_t i = 0; i < data.size(); i++) {
		auto& arr = data[i];
		MtPrint("Server: Recv data " + to_string(i));

		{
			bool success = channel.RecvData(data_from_client);
			if (!success) {
				return false;
			}
		}

		if (arr != data_from_client) {
			return false;
		}

		MtPrint("Server: Send data " + to_string(i));
		{
			for_each(data_from_client.begin(), data_from_client.end(), [](auto& e) {e -= 1; });

			bool success = channel.SendData(data_from_client.data(), data_from_client.size());

			if (!success) {
				return false;
			}
		}
	}

	MtPrint("Server: Test success");
	return true;
}

TEST(Crypt_IntegrationTestsCheck, ClientServerProcessData) {
#ifdef _DEBUG 
	int milliseconds_timeout = 20000;
#else
	int milliseconds_timeout = 2000;
#endif

	ASSERT_TRUE(NCryptConnectionWs::WsInit());

	NCryptAuthRsa auth;
	vector<uint64_t> private_key, public_key;
	auth.Generate(64, private_key, public_key, N_BASIC_RG);

	vector<vector<uint8_t>> data = { GetDataSet(),GetDataSet(123),GetDataSet(23512),GetDataSet(21) };

	auto handle_server = std::async(std::launch::async,
		TestServer,
		private_key,
		data,
		make_shared<NCryptSymIdea>()
	);

	auto handle_client = std::async(std::launch::async,
		TestClient,
		public_key,
		data,
		make_shared<NCryptSymIdea>()
	);

	auto start_time = chrono::high_resolution_clock::now();

	auto status_server = handle_server.wait_for(chrono::milliseconds(milliseconds_timeout) - (chrono::high_resolution_clock::now() - start_time));
	auto status_client = handle_client.wait_for(chrono::milliseconds(milliseconds_timeout) - (chrono::high_resolution_clock::now() - start_time));

	ASSERT_TRUE(status_server == future_status::ready);
	ASSERT_TRUE(status_client == future_status::ready);
	if (status_server != future_status::ready || status_client != future_status::ready) {
		return;
	}

	bool server_success = handle_server.get();
	bool client_success = handle_client.get();

	ASSERT_TRUE(server_success);
	ASSERT_TRUE(client_success);
	NCryptConnectionWs::WsClear();
}

};
