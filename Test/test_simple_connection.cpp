#include "gtest/gtest.h"

#include "NCryptAuthRsa.h"
#include "NCryptHashSha3.h"
#include "NCryptSymIdea.h"
#include "NCryptSyncDiffHelm.h"
#include "NCryptConnectionWs.h"
#include "NCryptChannel.h"
#include "NCryptRandom.h"

#include <future>

#include "util.h"


using namespace std;
namespace {

bool TestClient(const vector<uint64_t>& public_key, vector<uint8_t>& data) {
	MtPrint("Client: Init");

	shared_ptr<NCryptAuthRsa> auth(new NCryptAuthRsa());
	auth->SetPublicKey(public_key);

	shared_ptr<NCryptSyncDiffHelm> sync(new NCryptSyncDiffHelm());
	sync->SetPrimeSize(500);
	sync->SetPrime(bigint("0x000d80b7ee5b8da724e173bc283eef9a010a85968b81f4fc998e9a848b349f9939d2591a655bd12a529e142e8c961bdbdf775d580ab1f1d953a21e5123c4fcfb"));
	
	NCryptStr str;

	str.auth = auth;
	str.connect = make_shared<NCryptConnectionWs>();
	str.hash = make_shared<NCryptHashSha3>();
	str.rg = make_shared<NRandomGenSeed>(data.size());
	str.symm = make_shared<NCryptSymIdea>();
	str.sync = sync;

	NCryptChannel channel(str);

	MtPrint("Client: Connect");
	{
		bool success = channel.Connect("127.0.0.1", 22);
		if (!success) {
			MtPrint("Client: Connect error");
			return false;
		}
	}

	MtPrint("Client: Sync key");
	{
		bool success = channel.SyncKey();
		if (!success) {
			MtPrint("Client: SyncKey error");
			return false;
		}
	}
	MtPrint("Client: Sync key : " + bigint(sync->GetResult(), 1).ToHex());

	MtPrint("Client: Send data");
	{
		bool success = channel.SendData(data.data(), data.size());
		if (!success) {
			MtPrint("Client: Send data error");
			return false;
		}
	}

	MtPrint("Client: Test success");
	return true;
}

NCryptStr CreateServerCryptStr(const vector<uint64_t>& private_key) {
	shared_ptr<NCryptAuthRsa> auth(new NCryptAuthRsa());
	auth->SetPrivateKey(private_key);

	shared_ptr<NCryptSyncDiffHelm> sync(new NCryptSyncDiffHelm());
	sync->SetPrimeSize(500);

	NCryptStr str;

	str.auth = auth;
	str.connect = make_shared<NCryptConnectionWs>();
	str.hash = make_shared<NCryptHashSha3>();
	str.rg = make_shared<NRandomGenSeed>();
	str.symm = make_shared<NCryptSymIdea>();
	str.sync = sync;

	return str;
}

bool TestServer(const vector<uint64_t>& private_key, size_t count_echo) {
	MtPrint("Server: Init");

	NCryptConnectionWs listener;
	listener.ListenClients(22);

	auto connection = shared_ptr<NCryptConnection>(listener.AcceptClient(10));

	{
		// This code can be moved to other thread
		NCryptStr str = CreateServerCryptStr(private_key);
		NCryptChannel channel(str);

		MtPrint("Server: Listen");
		{
			bool success = channel.ServerConnect(connection);
			if (success == false) {
				return false;
			}
		}

		MtPrint("Server: Sync key");
		{
			bool success = channel.SyncKey();
			if (!success) {
				return false;
			}
		}
		MtPrint("Server: Sync key : " + bigint(str.sync->GetResult(), 1).ToHex());

		vector<uint8_t> data_from_client;

		MtPrint("Server: Recv data");

		{
			bool success = channel.RecvData(data_from_client);
			if (!success) {
				return false;
			}
		}
	}

	MtPrint("Server: Test success");
	return true;
}

TEST(Crypt_IntegrationTestsCheck, EchoServer) {
#ifdef _DEBUG 
	int milliseconds_timeout = 20000;
#else
	int milliseconds_timeout = 2000;
#endif
	const size_t test_count = 1;

	ASSERT_TRUE(NCryptConnectionWs::WsInit());

	NCryptAuthRsa auth;
	vector<uint64_t> private_key, public_key;
	auth.Generate(64, private_key, public_key, N_BASIC_RG);

	vector<vector<uint8_t>> data;
	for (size_t i = 0; i < test_count; i++) {
		data.push_back(GetDataSet(1235+i*10, i));
	}

	auto handle_server = std::async(std::launch::async,
		TestServer,
		private_key,
		test_count
	);

	vector<future<bool>> handle_clients;
	for (size_t i = 0; i < test_count; i++) {
		handle_clients.push_back(std::async(std::launch::async,
			TestClient,
			public_key,
			data[i]
		));
	}

	auto start_time = chrono::high_resolution_clock::now();

	auto status_server = handle_server.wait_for(chrono::milliseconds(milliseconds_timeout) - (chrono::high_resolution_clock::now() - start_time));

	vector<future_status>status_clients(test_count);
	for (size_t i = 0; i < test_count; i++) {
		status_clients[i] = handle_clients[i].wait_for(chrono::milliseconds(milliseconds_timeout) - (chrono::high_resolution_clock::now() - start_time));
	}

	ASSERT_TRUE(status_server == future_status::ready);
	for (size_t i = 0; i < test_count; i++) {
		ASSERT_TRUE(status_clients[i] == future_status::ready);
	}

	bool server_success = handle_server.get();
	vector<bool> clients_success(test_count);
	for (size_t i = 0; i < test_count; i++) {
		clients_success[i] = handle_clients[i].get();
	}

	ASSERT_TRUE(server_success);
	for (size_t i = 0; i < test_count; i++) {
		ASSERT_TRUE(clients_success[i]);
	}
	NCryptConnectionWs::WsClear();
}

};