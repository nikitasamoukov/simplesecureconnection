#include <mutex>
#include <iostream>

#include "util.h"

vector<uint8_t> GetDataSet(size_t size, uint64_t seed) {
	vector<uint8_t> data(size);
	for (size_t i = 0; i < size; i++) {
		data[i] = (char)(i * (i + seed));
	}
	return data;
}

void MtPrint(const string& message) {
	static mutex m;
	lock_guard<mutex> lock(m);
	cout << message << endl;
}

vector<uint64_t> ConvertVector8To64(const vector<uint8_t>& data8) {
	vector<uint64_t> data64((data8.size() + 7) / 8);
	for (size_t i = 0; i < data8.size(); i++) {
		((int8_t*)data64.data())[i] = data8[i];
	}
	return data64;
}

pair<char, char> ByteToHex(unsigned char b) {
	char hex_chars[16] = { '0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f' };
	return { hex_chars[b >> 4],hex_chars[b & ((1 << 4) - 1)] };
}