#pragma once

#include <vector>
#include <string>

using namespace std;

vector<uint8_t> GetDataSet(size_t size = 10000, uint64_t seed = 0);

void MtPrint(const string& message);

vector<uint64_t> ConvertVector8To64(const vector<uint8_t>& data8);
pair<char, char> ByteToHex(unsigned char b);